<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware g
roup. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.content.landing');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
Route::get('/rekap', [App\Http\Controllers\HomeController::class, 'rekap'])->name('rekap');
Route::get('/kualitas', [App\Http\Controllers\HomeController::class, 'kualitas'])->name('kualitas');
Route::get('/peternak/{peternak_id}', [App\Http\Controllers\HomeController::class, 'peternak'])->name('peternak');
Route::get('/test', [App\Http\Controllers\HomeController::class, 'test'])->name('test');
Route::get('/peternalList', [App\Http\Controllers\PeternakController::class, 'list'])->name('peternakList');
Route::get('/inputLitter', [App\Http\Controllers\PeternakController::class, 'inputLitter'])->name('inputLitter');
Route::post('/inputLitterStore', [App\Http\Controllers\PeternakController::class, 'inputLitterStore'])->name('inputLitterStore');
Route::get('/inputLitterEdit/{id}', [App\Http\Controllers\PeternakController::class, 'inputLitterEdit'])->name('inputLitterEdit');
Route::post('/inputLitterUpdate/{id}', [App\Http\Controllers\PeternakController::class, 'inputLitterUpdate'])->name('inputLitterUpdate');
Route::get('/biodataPeternak', [App\Http\Controllers\PeternakController::class, 'biodataPeternak'])->name('biodataPeternak');
Route::get('/kmen', [App\Http\Controllers\PeternakController::class, 'kmen'])->name('kmen');
Route::get('/cek_firebase', [App\Http\Controllers\HomeController::class, 'cek_firebase'])->name('cek_firebase');

Route::prefix('peternak')->name('peternak.')->group(function() {
    Route::get('/lists', [App\Http\Controllers\PeternakController::class, 'list'])->name('list');

    Route::get('/index', [App\Http\Controllers\PeternakController::class, 'index'])->name('index');
    Route::post('/store', [App\Http\Controllers\PeternakController::class, 'store'])->name('store');
    Route::post('/update/{id}', [App\Http\Controllers\PeternakController::class, 'update'])->name('update');

});
