@extends('admin.app')

@section('content')


<div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Form Carousel</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{($data!=null) ? route('admin.auth.carousel.update',$data->id) : route('admin.auth.carousel.store')}}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    @if($data!=null)
                                        @method('put')
                                    @endif

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Name</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{($data!=null) ? $data->name : ''}}">
                                            @error('name')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Image</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" name="image" id="image">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Desc</label>
                                        <div class="col-sm-12 col-md-7">
                                            <textarea class="form-control @error('name') is-invalid @enderror"  name="desc" style="height: 100px">{{($data!=null) ? $data->desc : ''}}</textarea>
{{--                                            <input type="text" name="desc" id="desc" class="form-control @error('name') is-invalid @enderror" value="{{($data!=null) ? $data->name : ''}}">--}}

                                            @error('desc')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

    </script>
@endsection
