@extends('admin.app')

@section('content')


    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Biodata Peternak</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{($data==null) ? route('peternak.store') : route('peternak.update',$data->id) }}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Luas Peternak</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="luas_peternakan" id="luas_peternakan" class="form-control @error('luas_peternakan') is-invalid @enderror" value="{{($data!=null) ? $data->luas_peternakan : ''}}">
                                            @error('luas_peternakan')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jumlah Sapi </label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="jumlah_sapi" id="jumlah_sapi" class="form-control @error('jumlah_sapi') is-invalid @enderror" value="{{($data!=null) ? $data->jumlah_sapi : ''}}">

                                            @error('jumlah_sapi')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

    </script>
@endsection
