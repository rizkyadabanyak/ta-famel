@extends('admin.app')

@section('content')


    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>List Peternak</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="item">
                                        <thead>
                                        <tr>
{{--                                            <th>#</th>--}}
                                            <th>code_peternak</th>
                                            <th>nama_peternakan</th>
                                            <th>Luas peternakan</th>
                                            <th>Jumlah Sapi</th>
                                            {{--                                        <th>Status</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $a)
                                        <tr>
                                            <td>{{$a->code_peternak}}</td>
                                            <td>{{$a->nama_peternakan}}</td>
                                            <td>{{$a->inputPeternak[0]->luas_peternakan}}</td>
                                            <td>{{$a->inputPeternak[0]->jumlah_sapi}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

    </script>
@endsection
