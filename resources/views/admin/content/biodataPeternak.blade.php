@extends('admin.app')

@section('content')


    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>List Peternak</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{($data==null) ? route('inputLitterStore') : route('inputLitterUpdate',$data->id)}}" method="post" enctype="multipart/form-data">
                                    {{--            <form action="{{route('adminMatkul.subject.update',)}}" method="post" enctype="multipart/form-data">--}}
                                    @csrf

                                    {{--                                    @if($data!=null)--}}
                                    {{--                                        @method('put')--}}
                                    {{--                                    @endif--}}

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Luas peternak </label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="number" name="litter_pagi" id="litter_pagi" class="form-control @error('litter_pagi') is-invalid @enderror" value="{{($data!=null) ? $data->luas_peternakan : old('litter_pagi')}}">
                                            @error('litter_pagi')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">jumlah sapi</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="number" name="litter_sore" id="litter_sore" class="form-control @error('litter_sore') is-invalid @enderror" value="{{($data!=null) ? $data->jumlah_sapi : old('litter_sore')}}">
                                            @error('litter_sore')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                    </div>


                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

    </script>
@endsection
