<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>KUD Lekok</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" type="text/css" />
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
{{--    <link href="css/styles.css" rel="stylesheet" />--}}
    <link href="{{asset('local/css/styles.css')}}" rel="stylesheet" />
</head>
<body>
<!-- Navigation-->
{{-- <nav class="navbar navbar-light bg-light static-top">
    <div class="container-fluid">
        <img src="{{asset('local/logonew.png')}}" width="79px" style="padding-left:20pt " alt="">
        <a class="navbar-brand" href="#!">RG Tasty</a>
        <a class="btn btn-primary" href="{{route('login')}}">Login</a>
    </div>
</nav> --}}
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav" style="padding-right: 40px;padding-left : 40px;background: #00000075;height:70px">
        <div class="container-fluid">
            <a class="navbar-brand" href="#page-top" style="color: white">
               RG
            </a>
                {{-- <img src="{{asset('local/logonew.png')}}" width="79px" style="padding-left:20pt " alt="">  --}}
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars ms-1"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                    <li class="nav-item"><a class="nav-link" href="#services">Services</a></li>
                    <li class="nav-item"><a class="nav-link" href="#portfolio">Portfolio</a></li>
                    <li class="nav-item"><a class="nav-link" href="#about">About</a></li>
                    <li class="nav-item"><a class="nav-link" href="#maps">Maps</a></li>
                    <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>
    <!-- Masthead-->
    <header class="masthead">
        <div class="container position-relative">
            <div class="row justify-content-center">
                <div class="col-xl-6">
                    <div class="text-center text-white">
                        <!-- Page heading-->
                        {{-- <h1 >KOPERASI UNIT DESA</h1>
                        <h3 class="mb-5">Koperasi Susu Kecamatan</h3> --}}
                        <div class="masthead-subheading">Welcome To Koperasi Susu Lekok!</div>
                        <div class="masthead-heading text-uppercase">It's Nice To Meet You</div>
                        <a class="btn btn-primary btn-xl text-uppercase"  href="{{route('login')}}">Login</a>
                        {{-- <a class="btn btn-primary" href="{{route('login')}}">Login</a> --}}
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Icons Grid-->
    {{-- <section class="features-icons bg-light text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                        <div class="features-icons-icon d-flex"><i class="bi-window m-auto text-primary"></i></div>
                        <h3>Fully Responsive</h3>
                        <p class="lead mb-0">Menjawab dan merekap semua datamu!</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                        <div class="features-icons-icon d-flex"><i class="bi-layers m-auto text-primary"></i></div>
                        <h3>Data Tersimpan Aman</h3>
                        <p class="lead mb-0">Data akan disimpan dalam database sehingga tidak perlu takut untuk kehilangan data!</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                        <div class="features-icons-icon d-flex"><i class="bi-terminal m-auto text-primary"></i></div>
                        <h3>Easy to Use</h3>
                        <p class="lead mb-0"> Siap untuk digunakan dan edit data sesuai yang kamu miliki!</p>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
    <!-- Services-->
    <section class="page-section" id="services">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Services</h2>
                <h3 class="section-subheading text-muted">Kami Menyiapkan Segala Keperluan Anda!</h3>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    {{-- <span class="fa-stack fa-4x">
                        <i class="fas fa-circle fa-stack-2x text-primary"></i>
                        <i class="fas fa-shopping-cart fa-stack-1x fa-inverse"></i>
                    </span> --}}
                    <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" fill="currentColor" class="bi bi-phone" viewBox="0 0 16 16">
                        <path d="M11 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h6zM5 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H5z"/>
                        <path d="M8 14a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                      </svg>
                    <h4 class="my-3">E-Data</h4>
                    <p class="text-muted">Anda bisa memasukkan data terbaru dari mana saja dan kapan saja!</p>
                </div>
                <div class="col-md-4">
                    <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" fill="currentColor" class="bi bi-layout-text-window-reverse" viewBox="0 0 16 16">
                        <path d="M13 6.5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5zm0 3a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5zm-.5 2.5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1 0-1h5z"/>
                        <path d="M14 0a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12zM2 1a1 1 0 0 0-1 1v1h14V2a1 1 0 0 0-1-1H2zM1 4v10a1 1 0 0 0 1 1h2V4H1zm4 0v11h9a1 1 0 0 0 1-1V4H5z"/>
                      </svg>
                    <h4 class="my-3">Responsive Design</h4>
                    <p class="text-muted">Tampilan yang menarik dan mudah digunakan sangat membantu untuk dipahami seluruh kalangan masyarakat.</p>
                </div>
                <div class="col-md-4">
                    <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" fill="currentColor" class="bi bi-laptop" viewBox="0 0 16 16">
                        <path d="M13.5 3a.5.5 0 0 1 .5.5V11H2V3.5a.5.5 0 0 1 .5-.5h11zm-11-1A1.5 1.5 0 0 0 1 3.5V12h14V3.5A1.5 1.5 0 0 0 13.5 2h-11zM0 12.5h16a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 12.5z"/>
                      </svg>
                    <h4 class="my-3">Actual Website</h4>
                    <p class="text-muted">Data terjamin aktual dan terkini sesuai dengan data harian dan data terupdate yang masuk.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- Portfolio Grid-->
    <section class="page-section bg-light" id="portfolio">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Portfolio</h2>
                <h3 class="section-subheading text-muted">Terpercaya dan kekinian memudahkan dalam proses perekapan!</h3>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-6 mb-4">
                    <!-- Portfolio item 1-->
                    <div class="portfolio-item">
                        <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal1">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src={{ asset('local/assets/img/information.jpg') }} alt="..." />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">Information</div>
                            <div class="portfolio-caption-subheading text-muted">Terupdate dan Mudah untuk Diakses!</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <!-- Portfolio item 2-->
                    <div class="portfolio-item">
                        <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal2">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="{{ asset('local/assets/img/inputdata.jpg') }}" alt="..." />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">Explore</div>
                            <div class="portfolio-caption-subheading text-muted">Lihat dan Masukkan Datamu Kapanpun dan Dimanapun!</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4">
                    <!-- Portfolio item 3-->
                    <div class="portfolio-item">
                        <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal3">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                            </div>
                            <img class="img-fluid" src="{{ asset('local/assets/img/rawData.png') }}" alt="..." />
                        </a>
                        <div class="portfolio-caption">
                            <div class="portfolio-caption-heading">Finish</div>
                            <div class="portfolio-caption-subheading text-muted">Data Recorded</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                </div>
            </div>
        </div>
    </section>
    {{-- <!-- About-->
    <section class="page-section" id="about">
        <div class="container">
            <div class="text-center">
                <h2 class="section-heading text-uppercase">Berita Terkini</h2>
                <h3 class="section-subheading text-muted">Pantau Terus Berita Terkini dari Peternakan</h3>
            </div>
            <div class="row align-items-center">
                <div class="col-md-3 col-sm-6 my-3">
                    <a href="https://nasional.sindonews.com/read/756951/15/dukung-perempuan-majukan-sektor-peternakan-sapi-perah-di-indonesia-1651133092"><img class="img-fluid img-brand d-block mx-auto" src="assets/img/logos/microsoft.svg" alt="..." aria-label="Microsoft Logo" /> Dukung Perempuan Indonesia Memajukan Sektor Sapi Perah!</a>
                </div>
                <div class="col-md-3 col-sm-6 my-3">
                    <a href="#!"><img class="img-fluid img-brand d-block mx-auto" src="assets/img/logos/google.svg" alt="..." aria-label="Google Logo" /></a>
                </div>
                <div class="col-md-3 col-sm-6 my-3">
                    <a href="#!"><img class="img-fluid img-brand d-block mx-auto" src="assets/img/logos/facebook.svg" alt="..." aria-label="Facebook Logo" /></a>
                </div>
                <div class="col-md-3 col-sm-6 my-3">
                    <a href="#!"><img class="img-fluid img-brand d-block mx-auto" src="assets/img/logos/ibm.svg" alt="..." aria-label="IBM Logo" /></a>
                </div>
            </div>
    </section> --}}
    {{-- about --}}
        <section class="page-section bg-light" id="portfolio">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Berita Terkini</h2>
                    <h3 class="section-subheading text-muted">Terkini dan Terhangat dari Dunia Peternakan</h3>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-6 mb-4">
                        <!-- Portfolio item 1-->
                        <div class="portfolio-item">
                            <a class="portfolio-link" data-bs-toggle="modal" href="https://nasional.sindonews.com/read/756951/15/dukung-perempuan-majukan-sektor-peternakan-sapi-perah-di-indonesia-1651133092">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                </div>
                                <img class="img-fluid" src="{{ asset('local/assets/img/perempuanindo.jpg') }}">
                            <div class="portfolio-caption">
                                <div class="portfolio-caption-heading">
                                    <a href = "https://nasional.sindonews.com/read/756951/15/dukung-perempuan-majukan-sektor-peternakan-sapi-perah-di-indonesia-1651133092" style="color: black; text-decoration:none" >
                                    Majukan Sektor Sapi Perah</div></a>
                                <div class="portfolio-caption-subheading text-muted">Dukung Perempuan Indonesia Majukan Sektor Peternakan Sapi Perah!</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-4">
                        <!-- Portfolio item 2-->
                        <div class="portfolio-item">
                            <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal2">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                </div>
                                <img class="img-fluid" src=" {{ asset('local/assets/img/kementan.jpeg') }}" alt="..." />
                            </a>
                            <div class="portfolio-caption">
                                <div class="portfolio-caption-heading">
                                    <a href="https://ditjenpkh.pertanian.go.id/berita/1340-kementan-berkomitmen-kembangkan-produksi-susu-segar-dalam-negeri" style="color: black; text-decoration:none">
                                    Tingkatkan Konsumsi Susu Sapi Perah!</div></a>
                                <div class="portfolio-caption-subheading text-muted">Kementan Berkomitmen Kembangkan Produksi Susu Segar Dalam Negeri</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-4">
                        <!-- Portfolio item 3-->
                        <div class="portfolio-item">
                            <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal3">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                </div>
                                <img class="img-fluid" src="{{ asset('local/assets/img/portof3.jpg') }}" alt="..." />
                            </a>
                            <div class="portfolio-caption">
                                <div class="portfolio-caption-heading">
                                    <a href="https://www.kompas.com/tag/sapi-perah"style="color: black; text-decoration:none">
                                    Kerjasama dan Investasi</div></a>
                                <div class="portfolio-caption-subheading text-muted">Indonesia Jajaki Investasi Penyediaan Sapi Perah dengan Belanda</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
                    </div>
                </div>
            </div>
        </section>

    <!--maps 1-->
    <section class="new-maps" id="maps" style="padding: 0rem 0;">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Find Our Location</h2>
            <h4> </h4>
        </div>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-apFSiZANM_Pn0EWbwMaUAT0NgjzQvnI"></script>
        <script>
        function initMap() {
            var myLatLng = {lat: -7.682814229107237, lng: 113.01193526968};

            var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: myLatLng
            });

            var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Your location'
            });
        }
        </script>

        <!-- Map container -->
        <div id="map" style="height: 500px"></div>

        <!-- Initialize map -->
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-apFSiZANM_Pn0EWbwMaUAT0NgjzQvnI&callback=initMap">
        </script>

    </section>
    <!-- Footer-->
    <footer class="footer bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 h-100 text-center text-lg-start my-auto">
                    <ul class="list-inline mb-2">
                        <li class="list-inline-item"><a href="#!">About</a></li>
                        <li class="list-inline-item">⋅</li>
                        <li class="list-inline-item"><a href="https://www.instagram.com/qows.indonesia/">Contact</a></li>
                        <li class="list-inline-item">⋅</li>
                        <li class="list-inline-item"><a href="#!">Terms of Use</a></li>
                        <li class="list-inline-item">⋅</li>
                        <li class="list-inline-item"><a href="#!">Privacy Policy</a></li>
                    </ul>
                    <p class="text-muted small mb-4 mb-lg-0">&copy; Your Website 2022. All Rights Reserved.</p>
                </div>
                <div class="col-lg-6 h-100 text-center text-lg-end my-auto">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item me-4">
                            <a href="#!"><i class="bi-facebook fs-3"></i></a>
                        </li>
                        <li class="list-inline-item me-4">
                            <a href="#!"><i class="bi-twitter fs-3"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.instagram.com/qows.indonesia/"><i class="bi-instagram fs-3"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="{{asset('local/js/scripts.js')}}"></script>
    {{--<script src="js/scripts.js"></script>--}}
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <!-- * *                               SB Forms JS                               * *-->
    <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>

</body>
</html>
