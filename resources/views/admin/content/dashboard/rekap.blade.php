@extends('admin.app')

@section('content')

    @push('style')
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/jqvmap/dist/jqvmap.min.css">

    @endpush

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Rekapp</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">List </h2>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="mb-3">{!! DNS2D::getBarcodeHTML('bagus', 'QRCODE') !!}</div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="https://demo.getstisla.com/assets/modules/chart.min.js"></script>

    @push('script')
        <script src="https://demo.getstisla.com/assets/modules/chart.min.js"></script>
        <script src="https://demo.getstisla.com/assets/modules/jqvmap/dist/jquery.vmap.min.js"></script>
        <script src="https://demo.getstisla.com/assets/modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="https://demo.getstisla.com/assets/js/scripts.js"></script>
{{--        <script src="https://demo.getstisla.com/assets/js/page/index-0.js"></script>--}}

        <script>

        </script>

    @endpush


{{--@push('script')--}}
{{--    <script type="text/javascript">--}}
{{--        $(document).ready(function(){--}}
{{--            $('#item').DataTable({--}}
{{--                processing: true,--}}
{{--                serverSide: true,--}}
{{--                ajax: {--}}
{{--                    url: "{{route('dashboard')}}",--}}
{{--                },--}}
{{--                columns: [--}}
{{--                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },--}}
{{--                    {--}}
{{--                        data: 'name_team',--}}
{{--                        name: 'name_team'--}}
{{--                    },--}}
{{--                    {--}}
{{--                        data: 'ranking',--}}
{{--                        name: 'ranking'--}}
{{--                    },{--}}
{{--                        data: 'score',--}}
{{--                        name: 'score'--}}
{{--                    },--}}
{{--                ]--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}

{{--@endpush--}}

@endsection
