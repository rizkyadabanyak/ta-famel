@extends('admin.app')

@section('content')

    @push('style')
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/jqvmap/dist/jqvmap.min.css">

    @endpush

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Dashboard</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">List </h2>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <br><br>

                            <div class="table-responsive">
                                <table class="table table-striped" id="item">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Liter_Pagi</th>
                                        <th>Kualitas_Pagi</th>
                                        <th>Liter_Sore</th>
                                        <th>Kualitas_Sore</th>
                                        <th>Liter_Total</th>
                                        <th>Kualitas_Total</th>
{{--                                        <th>Status</th>--}}
                                    </tr>
                                    </thead>

                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="https://demo.getstisla.com/assets/modules/chart.min.js"></script>

    @push('script')
        <script src="https://demo.getstisla.com/assets/modules/chart.min.js"></script>
        <script src="https://demo.getstisla.com/assets/modules/jqvmap/dist/jquery.vmap.min.js"></script>
        <script src="https://demo.getstisla.com/assets/modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="https://demo.getstisla.com/assets/js/scripts.js"></script>
{{--        <script src="https://demo.getstisla.com/assets/js/page/index-0.js"></script>--}}

        <script>


            $('#item').DataTable({

                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{route('dashboard')}}",
                },
                dom:'lBfrtip',
                buttons: ['excel','pdf'],
                "lengthMenu": [50,100,500,1000],
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'Date',
                        name: 'Date'
                    },
                    {
                        data: 'Liter_Pagi',
                        name: 'Liter_Pagi'
                    },
                    {
                        data: 'Kualitas_Pagi',
                        name: 'Kualitas_Pagi'
                    },
                    {
                        data: 'Liter_Sore',
                        name: 'Liter_Sore'
                    },
                    {
                        data: 'Kualitas_Sore',
                        name: 'Kualitas_Sore'
                    },{
                        data: 'Liter_Total',
                        name: 'Liter_Total'
                    },{
                        data: 'Kualitas_Total',
                        name: 'Kualitas_Total'
                    },
                ]
            });

        </script>

    @endpush


{{--@push('script')--}}
{{--    <script type="text/javascript">--}}
{{--        $(document).ready(function(){--}}
{{--            $('#item').DataTable({--}}
{{--                processing: true,--}}
{{--                serverSide: true,--}}
{{--                ajax: {--}}
{{--                    url: "{{route('dashboard')}}",--}}
{{--                },--}}
{{--                columns: [--}}
{{--                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },--}}
{{--                    {--}}
{{--                        data: 'name_team',--}}
{{--                        name: 'name_team'--}}
{{--                    },--}}
{{--                    {--}}
{{--                        data: 'ranking',--}}
{{--                        name: 'ranking'--}}
{{--                    },{--}}
{{--                        data: 'score',--}}
{{--                        name: 'score'--}}
{{--                    },--}}
{{--                ]--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}

{{--@endpush--}}

@endsection
