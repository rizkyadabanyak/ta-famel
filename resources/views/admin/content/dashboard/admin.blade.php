@extends('admin.app')

@section('content')

    @push('style')
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/jqvmap/dist/jqvmap.min.css">

    @endpush

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <canvas id="chart2" width="50" height="10"></canvas>

        </div>
        <form action="{{route('dashboard')}}" method="GET">
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-body">

                            {{--                                @csrf--}}
                            {{--                                @method('get')--}}
                            <center>
                            <h4>Start</h4> </center>
                            <input type="date" class="form-control" name="start_date" value="{{$start_date}}">

                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-body">

                            {{--                                @csrf--}}
                            {{--                                @method('get')--}}
                            <center>
                            <h4>End</h4> </center>
                            <input type="date" class="form-control" name="end_date" value="{{$end_date}}">

                    </div>
                </div>
            </div>
        </div>
        <center>
            <button type="submit" class="btn btn-success">Filter</button>
        </center>
            <br>
    </form>
        <div class="section-body">



            <div class="row">
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <center>

                                <h1>Baik</h1>
                            </center>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nama</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($kmean_baik as $vs)
                                <tr>
                                    <th scope="row">1</th>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $vs[0] }}</td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <center>

                                <h1>Sedang</h1>
                            </center>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nama</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($kmean_sedang as $s)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $s[0] }}</td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <center>

                                <h1>Buruk</h1>
                            </center>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nama</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($kmean_buruk as $s)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $s[0] }}</td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <br><br>

                            <div class="table-responsive">
                                <table class="table table-striped" id="item">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Liter_Pagi</th>
                                        <th>Kualitas_Pagi</th>
                                        <th>Liter_Sore</th>
                                        <th>Kualitas_Sore</th>
                                        <th>Liter_Total</th>
                                        <th>Kualitas_Total</th>
{{--                                        <th>Status</th>--}}
                                    </tr>
                                    </thead>

                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="https://demo.getstisla.com/assets/modules/chart.min.js"></script>

    @push('script')
        <script src="https://demo.getstisla.com/assets/modules/chart.min.js"></script>
        <script src="https://demo.getstisla.com/assets/modules/jqvmap/dist/jquery.vmap.min.js"></script>
        <script src="https://demo.getstisla.com/assets/modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="https://demo.getstisla.com/assets/js/scripts.js"></script>
{{--        <script src="https://demo.getstisla.com/assets/js/page/index-0.js"></script>--}}

        <script>
            const DATA_COUNT = 7;
            const NUMBER_CFG = {count: DATA_COUNT, min: -100, max: 100};

            const data = {
                labels: ['Desember','januari','februari','maret','april'],
                datasets: [
                    {
                        label: 'Baik',
                        data: [
                            4,3,5,6,7
                        ],
                        borderColor: '#0000ff',
                        backgroundColor:  '#0000ff',
                    },
                    {
                        label: 'Sedang',
                        data: [
                           4,3,3,2,2
                        ],
                        borderColor:  '#ffb700',
                        backgroundColor:  '#ffb700',
                    },
                    {
                        label: 'Buruk',
                        data: [
                            2,4,2,2,1
                        ],
                        borderColor:  '#a8a8a8',
                        backgroundColor:  '#a8a8a8',
                    }
                ]
            };

            const ctx = document.getElementById('chart2');

            new Chart(ctx, {
                type: 'bar',
                data: data,
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Chart.js Bar Chart'
                        }
                    }
                },
            });


            $('#item').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{route('dashboard')}}",
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'Date',
                        name: 'Date'
                    },
                    {
                        data: 'Liter_Pagi',
                        name: 'Liter_Pagi'
                    },
                    {
                        data: 'Kualitas_Pagi',
                        name: 'Kualitas_Pagi'
                    },
                    {
                        data: 'Liter_Sore',
                        name: 'Liter_Sore'
                    },
                    {
                        data: 'Kualitas_Sore',
                        name: 'Kualitas_Sore'
                    },{
                        data: 'Liter_Total',
                        name: 'Liter_Total'
                    },{
                        data: 'Kualitas_Total',
                        name: 'Kualitas_Total'
                    },
                ]
            });

        </script>

    @endpush


{{--@push('script')--}}
{{--    <script type="text/javascript">--}}
{{--        $(document).ready(function(){--}}
{{--            $('#item').DataTable({--}}
{{--                processing: true,--}}
{{--                serverSide: true,--}}
{{--                ajax: {--}}
{{--                    url: "{{route('dashboard')}}",--}}
{{--                },--}}
{{--                columns: [--}}
{{--                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },--}}
{{--                    {--}}
{{--                        data: 'name_team',--}}
{{--                        name: 'name_team'--}}
{{--                    },--}}
{{--                    {--}}
{{--                        data: 'ranking',--}}
{{--                        name: 'ranking'--}}
{{--                    },{--}}
{{--                        data: 'score',--}}
{{--                        name: 'score'--}}
{{--                    },--}}
{{--                ]--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}

{{--@endpush--}}

@endsection
