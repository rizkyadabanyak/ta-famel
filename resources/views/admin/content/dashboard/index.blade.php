@extends('admin.app')

@section('content')

    @push('style')
        <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/jqvmap/dist/jqvmap.min.css">

    @endpush

    <img src="{{asset('fjfyklkn')}}">
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Dashboard</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">List </h2>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('dashboard')}}" method="GET">
{{--                                @csrf--}}
{{--                                @method('get')--}}
                                <h5>Start</h5>
                                <input type="date" value="{{$start_date}}" class="form-control" name="start_date">
                                <h5>End</h5>
                                <input type="date" value="{{$end_date}}" class="form-control" name="end_date"> <br>
                                <center>
                                <button type="submit" class="btn btn-success">Filter</button>
                                </center>
                                <br>
                            </form>

                            <h5>Total Pendapatan : Rp.{{$total_harga}} </h5>
                            <h5>Total Kualitas Ternak: Sedang</h5>
                            <br><br>
                            <div class="row">
                                <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h3>Harga</h3>
                                            <canvas id="chart1" height="182"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-12 col-sm-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h3>Kualitas</h3>
                                            <canvas id="chart2" height="182"></canvas>
                                        </div>
                                    </div>
                                </div>

                            </div>




                            <div class="table-responsive">
                                <table class="table table-striped" id="item">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Liter_Pagi</th>
                                        <th>Kualitas_Pagi</th>
                                        <th>Liter_Sore</th>
                                        <th>Kualitas_Sore</th>
                                        <th>Liter_Total</th>
                                        <th>Kualitas_Total</th>
{{--                                        <th>Status</th>--}}
                                    </tr>
                                    </thead>

                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="https://demo.getstisla.com/assets/modules/chart.min.js"></script>

    @push('script')
        <script src="https://demo.getstisla.com/assets/modules/chart.min.js"></script>
        <script src="https://demo.getstisla.com/assets/modules/jqvmap/dist/jquery.vmap.min.js"></script>
        <script src="https://demo.getstisla.com/assets/modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="https://demo.getstisla.com/assets/js/scripts.js"></script>
{{--        <script src="https://demo.getstisla.com/assets/js/page/index-0.js"></script>--}}

        <script>
            var statistics_chart1 = document.getElementById("chart1").getContext('2d');
            var statistics_chart2 = document.getElementById("chart2").getContext('2d');

            var myChart1 = new Chart(statistics_chart1, {
                type: 'line',
                data: {
                    // labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                    labels: [
                        @foreach($data as $a)
                        "{{$a['tgl']}}",
                        @endforeach
                        ],
                    datasets: [{
                        label: 'Statistics',
                        data: [
                            @foreach($data as $a)
                                "{{$a['harga']}}",
                            @endforeach
                        ],
                        borderWidth: 5,
                        borderColor: '#e30000',
                        backgroundColor: 'transparent',
                        pointBackgroundColor: '#fff',
                        pointBorderColor: '#e30000',
                        pointRadius: 4
                    }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            gridLines: {
                                display: false,
                                drawBorder: false,
                            },
                            ticks: {
                                stepSize: 150
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                color: '#fbfbfb',
                                lineWidth: 2
                            }
                        }]
                    },
                }
            });

            var yLabels = {
                1 : 'buruk',
                2 : 'sedang',
                3 : 'bagus'
            }
            var myChart2 = new Chart(statistics_chart2, {
                type: 'line',
                data: {
                    labels: [
                        @foreach($data as $a)
                            "{{$a['tgl']}}",
                        @endforeach
                    ],datasets: [{
                        label: 'Statistics',
                        data: [
                            @foreach($data as $a)
                                '{{$a['totalScore']}}',
                            @endforeach
                        ],
                        borderWidth: 5,
                        borderColor: '#6777ef',
                        backgroundColor: 'transparent',
                        pointBackgroundColor: '#fff',
                        pointBorderColor: '#6777ef',
                        pointRadius: 4
                    }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                callback: function(value, index, values) {
                                    return yLabels[value];
                                }
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                color: '#fbfbfb',
                                lineWidth: 2
                            }
                        }]
                    },
                }
            });


            $('#item').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{route('dashboard')}}",
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                    {
                        data: 'Date',
                        name: 'Date'
                    },
                    {
                        data: 'Liter_Pagi',
                        name: 'Liter_Pagi'
                    },
                    {
                        data: 'Kualitas_Pagi',
                        name: 'Kualitas_Pagi'
                    },
                    {
                        data: 'Liter_Sore',
                        name: 'Liter_Sore'
                    },
                    {
                        data: 'Kualitas_Sore',
                        name: 'Kualitas_Sore'
                    },{
                        data: 'Liter_Total',
                        name: 'Liter_Total'
                    },{
                        data: 'Kualitas_Total',
                        name: 'Kualitas_Total'
                    },
                ]
            });

        </script>

    @endpush


{{--@push('script')--}}
{{--    <script type="text/javascript">--}}
{{--        $(document).ready(function(){--}}
{{--            $('#item').DataTable({--}}
{{--                processing: true,--}}
{{--                serverSide: true,--}}
{{--                ajax: {--}}
{{--                    url: "{{route('dashboard')}}",--}}
{{--                },--}}
{{--                columns: [--}}
{{--                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },--}}
{{--                    {--}}
{{--                        data: 'name_team',--}}
{{--                        name: 'name_team'--}}
{{--                    },--}}
{{--                    {--}}
{{--                        data: 'ranking',--}}
{{--                        name: 'ranking'--}}
{{--                    },{--}}
{{--                        data: 'score',--}}
{{--                        name: 'score'--}}
{{--                    },--}}
{{--                ]--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}

{{--@endpush--}}

@endsection
