@extends('admin.app')

@section('content')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />

    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>


    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>input litter Peternak</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <form action="{{($data==null) ? route('inputLitterStore') : route('inputLitterUpdate',$data->id)}}" method="post" enctype="multipart/form-data">
                                    {{--            <form action="{{route('adminMatkul.subject.update',)}}" method="post" enctype="multipart/form-data">--}}
                                    @csrf

{{--                                    @if($data!=null)--}}
{{--                                        @method('put')--}}
{{--                                    @endif--}}

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">date</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="date" name="date" id="date" class="form-control @error('code') is-invalid @enderror" value="{{($data!=null) ? $data->Date : old('date')}}">
                                            @error('date')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Litter pagi </label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="number" name="litter_pagi" id="litter_pagi" class="form-control @error('litter_pagi') is-invalid @enderror" value="{{($data!=null) ? $data->Liter_Pagi : old('litter_pagi')}}">
                                            @error('litter_pagi')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">litter sore</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="number" name="litter_sore" id="litter_sore" class="form-control @error('litter_sore') is-invalid @enderror" value="{{($data!=null) ? $data->Liter_Sore : old('litter_sore')}}">
                                            @error('litter_sore')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Peternak</label>
                                        <div class="col-sm-12 col-md-7">
                                            {{--                        <input type="text" name="name_en" id="name_en" class="form-control @error('name_en') is-invalid @enderror" value="{{($data!=null) ? $data->name_en : old('name_en')}}">--}}
                                            <select name="peternak_id" id="peternak_id" class="form-control @error('peternak_id') is-invalid @enderror">
                                                @if($data)
                                                    <option value="{{$data->peternakan->id}}">{{$data->peternakan->nama_peternakan}}</option>

                                                @endif
                                                @foreach($peternak as $a)
                                                    <option value="{{$a->id}}">{{$a->nama_peternakan}}</option>
                                                @endforeach
                                            </select>
                                            @error('prodi_id')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                    </div>


                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">
                                                <i class="fa fa-save"></i> Save
                                            </button>
                                        </div>
                                    </div>
                                </form>


                                <br>
                                <div class="table-responsive">
                                    <table class="table table-striped" id="item" >
                                        <thead>
                                        <tr>
{{--                                            <th>#</th>--}}
                                            <th>nama_peternakan</th>
                                            <th>Date</th>
                                            <th>litter pagi</th>
                                            <th>litter sore</th>
                                            <th>Action</th>
                                            {{--                                        <th>Status</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($item as $a)
                                        <tr>
{{--                                            <td>{{$a->code_peternak}}</td>--}}
                                            <td>{{$a->peternakan->nama_peternakan}}</td>
                                            <td>{{$a->Date}}</td>
                                            <td>{{$a->Liter_Pagi}}</td>
                                            <td>{{$a->Liter_Sore}}</td>
                                            <td><a class="btn btn-info" href="{{route('inputLitterEdit',$a->id)}}">Edit</a></td>

                                        </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $('#item').DataTable();

    </script>
@endsection
