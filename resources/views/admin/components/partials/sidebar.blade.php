<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
{{--                <a href="index.html">TA</a>--}}
            <img src="{{asset('local/STIKERQOWS.png')}}" width="50" alt="">
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <img src="{{asset('local/STIKERQOWS.png')}}" width="50" alt="">
        </div>
        @if(\Illuminate\Support\Facades\Auth::user()->level == 'admin')
            <ul class="sidebar-menu">

                <li class="menu-header">Dashboard</li>
                <li><a class="nav-link" href="{{route('dashboard')}}"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
                <li><a class="nav-link" href="{{route('peternakList')}}"><i class="fas fa-edit"></i> <span>Peternak</span></a></li>
                <li><a class="nav-link" href="{{route('inputLitter')}}"><i class="fas fa-edit"></i> <span>Input Litter</span></a></li>
                <li><a class="nav-link" href="{{route('kualitas')}}"><i class="fas fa-edit"></i> <span>Kualitas Peternak</span></a></li>
                <li><a class="nav-link" href="{{route('rekap')}}"><i class="fas fa-edit"></i> <span>Rekap</span></a></li>

                {{--                <li class="nav-item dropdown">--}}
                {{--                    <a href="#" class="nav-link has-dropdown"><i class="fas fa-th-large"></i><span>Master</span></a>--}}
                {{--                    <ul class="dropdown-menu">--}}
                {{--                        <li><a class="nav-link" href="{{route('admin.faqs.index')}}">Faqs</a></li>--}}
                {{--                        <li><a class="nav-link" href="{{route('admin.descriptions.index')}}">Descriptions</a></li>--}}
                {{--                        <li><a class="nav-link" href="{{route('admin.pedoman.index')}}">Pedoman</a></li>--}}
                {{--                        <li><a class="nav-link" href="{{route('admin.contacts.index')}}">Contact</a></li>--}}
                {{--                        <li><a class="nav-link" href="{{route('admin.sponsors.index')}}">Sponsor</a></li>--}}
                {{--                        <li><a class="nav-link" href="{{route('admin.testy.index')}}">Testimoni</a></li>--}}
                {{--                        <li><a class="nav-link" href="{{route('admin.documentation.index')}}">Documentation</a></li>--}}
                {{--                    </ul>--}}
                {{--                </li>--}}


            </ul>
        @elseif(auth()->user()->level == 'peternak')
            <ul class="sidebar-menu">

                <li><a class="nav-link" href="{{route('dashboard')}}"><i class="fas fa-home"></i> <span>Peternak</span></a></li>
                <li><a class="nav-link" href="{{route('biodataPeternak')}}"><i class="fas fa-home"></i> <span>Bio Data Pertekanan</span></a></li>
            </ul>

        @else
            <ul class="sidebar-menu">
                <li class="menu-header">Dashboard</li>
{{--                <li><a class="nav-link" href="{{route('admin.referrals.index')}}"><i class="fas fa-fire"></i> <span>Referral</span></a></li>--}}
                <li><a class="nav-link" href=""><i class="fas fa-fire"></i> <span>Pengguna</span></a></li>
            </ul>
        @endif
{{--        @if(Auth::user()->role == 0)--}}

    </aside>
</div>
