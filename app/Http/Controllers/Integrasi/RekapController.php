<?php

namespace App\Http\Controllers\Integrasi;

use App\Http\Controllers\Controller;
use App\Http\Requests\RekapRequest;
use App\Models\Integrtasi\Rekap;
use App\Models\Integrtasi\Rute;
use Carbon\Carbon;
use Illuminate\Http\Request;
use SebastianBergmann\Diff\Exception;

class RekapController extends Controller
{
    public function store (RekapRequest $request){
//        dd('sss');

//        $request->validate([
//           'nama_jalan' => ['required']
//        ]);
        try {
            $data = new Rekap();

            $data->rute_id = $request->rute_id;
            $data->nama_jalan = $request->nama_jalan;
            $data->nama_toko = $request->nama_toko;
            $data->gambar = $request->gambar;
            $data->catatan = $request->catatan;
            $data->sent_status = $request->sent_status;
            $data->waktu_kirim = $request->waktu_kirim;

            $data->save();


            return response()->json([
                'status' => 'success',
                'data' => $data,
                'message' => 'success create data'
            ],200);

        }catch (Exception $exception){
            return response()->json([
                'status' => false,
                'data' => $exception,
                'message' => 'error create data'
            ],200);
        }

    }

    public function storeRute (Request $request){
//        dd('sss');

//        $request->validate([
//           'nama_jalan' => ['required']
//        ]);
        try {
            $data = new Rute();

            $data->date = Carbon::now();
            $data->name_rute = $request->name_rute;

            $data->save();


            return response()->json([
                'status' => 'success',
                'data' => $data,
                'message' => 'success create data'
            ],200);

        }catch (Exception $exception){
            return response()->json([
                'status' => false,
                'data' => $exception,
                'message' => 'error create data'
            ],200);
        }

    }
}
