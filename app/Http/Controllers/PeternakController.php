<?php

namespace App\Http\Controllers;

use App\Models\inputPeternak;
use App\Models\KualitasPeternak;
use App\Models\Liter;
use App\Models\Peternakan;
use Illuminate\Http\Request;
use KMeans\Space;

class PeternakController extends Controller
{

    public function biodataPeternak(Request $request){
        $data = inputPeternak::where('peternakan_id',auth()->user()->peternakan->id)->first();
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.biodataPeternak');

    }

    public function inputLitter(){

        $item = KualitasPeternak::with(['peternakan'])->get();

        $peternak = Peternakan::all();
        $data = null;


        view()->share([
            'item' => $item,
            'data' => $data,
            'peternak' => $peternak
        ]);

        return view('admin.content.inputLitter');
    }

    public function inputLitterUpdate ($id,Request $request){
        $data = Liter::where('id',$id)->first();
        $data->peternakan_id = $request->peternak_id;
        $data->Date = $request->date;
        $data->Liter_Pagi = $request->litter_pagi;
        $data->Liter_Sore = $request->litter_sore;

        $data->save();

        return redirect()->route('inputLitter');
    }

    public function inputLitterEdit ($id){
//        dd($id);
        $item = KualitasPeternak::with(['peternakan'])->get();

        $peternak = Peternakan::all();
        $data = KualitasPeternak::with(['peternakan'])->where('id',$id)->first();


        view()->share([
            'item' => $item,
            'data' => $data,
            'peternak' => $peternak
        ]);

        return view('admin.content.inputLitter');
    }

    public function inputLitterStore(Request $request){

//        dd($request->date);
        $data = new Liter();
        $data->peternakan_id = $request->peternak_id;
        $data->Date = $request->date;
        $data->Liter_Pagi = $request->litter_pagi;
        $data->Liter_Sore = $request->litter_sore;

        $data->save();

        return redirect()->back();

    }


    public function list(){
//        dd('dqw');
        $data = Peternakan::with('inputPeternak')->get();

//        dd($data);
        view()->share([
            'data' => $data
        ]);

        return view('admin.content.listPeternak');
    }

    public function index(){
//        dd(auth()->user()->peternakan->id);
        $peternak_id = auth()->user()->peternakan->id;
        $data = inputPeternak::find($peternak_id);
//        dd($data);
        view()->share([
            'data' => $data
        ]);
        return view('admin.content.peternak.index');
    }
    public function store(){

    }

    public function update($id,Request $request){
        $data = inputPeternak::find($id);

        $data->luas_peternakan = $request->luas_peternakan;
        $data->jumlah_sapi = $request->jumlah_sapi;
        $data->save();

        return redirect()->back();
    }

    public function kmen(){
        $points = [
            [1,1],[2,2],[3,1],[3,2],[2,3],[1,5],[1,9],[1,9]
        ];

        $space = new Space(2);

// add points to space
        foreach ($points as $i => $coordinates) {
            $space->addPoint($coordinates);
        }

//        $clusters = $space->solve(2);

//        dd($clusters);
// display the cluster centers and attached points
//        foreach ($clusters as $num => $cluster) {
//            $coordinates = $cluster->getCoordinates();
//            printf(
//                "Cluster %s [%d,%d]: %d points\n",
//                $num,
//                $coordinates[0],
//                $coordinates[1],
//                count($cluster)
//            );
//        }

        $clusters = $space->solve(2, function($space, $clusters) {
            static $iterations = 0;

            printf("Iteration: %d\n", ++$iterations);

            foreach ($clusters as $i => $cluster) {
                printf("Cluster %d [%d,%d]: %d points\n", $i, $cluster[0], $cluster[1], count($cluster));
            }
        });
    }
}
