<?php

namespace App\Http\Controllers;

use App\Models\Deteksi;
use App\Models\inputPeternak;
use App\Models\KualitasPeternak;
use App\Models\Peternakan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
class HomeController extends Controller{


    public function rekap(){
//        dd('sss');
        return view('admin.content.dashboard.rekap');
    }

    public function test(){

        $now = Carbon::now();
        return;

        $data = Deteksi::where('peternakan_id',auth()->user()->peternakan->id)->whereDate('time', date('Y-m-d'))->get();
        foreach ($data as $a){
            echo $a->time.'<br>';
        }

        return;
        $nowLocal = date('d-m-Y H:i:s');
        $date = Carbon::parse($nowLocal);

//        dd($date->startOfWeek()->format('Y-m-d H:i'));
        dd($date->endOfWeek()->format('Y-m-d'));
    }


    public function cek_firebase(){

        $factory = (new Factory)->withServiceAccount(assert('tracowss-firebase-adminsdk-zeu3e-29d0243c7b.json'));

        dd($factory);
//        $database = $firebase->createDatabase();

        dd($database);
        $blog = $database
            ->getReference('blog');

        echo '<pre>';
        print_r($blog->getvalue());
        echo '</pre>';

    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function peternak(Request $request,$peternakan_id){
//        dd($peternakan_id);
        $peternak_id = $peternakan_id;
        $all_dates = [
            '2022-12-08'
        ];
        if ($request->start_date && $request->end_date){
            $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            $end_date = Carbon::parse(request()->end_date)->toDateTimeString();

            $data = Deteksi::where(function ($query) {
                $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
                $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
                $query->whereDate('time', '>=', $start_date)
                    ->whereDate('time', '<=', $end_date);
            })->where('peternakan_id',$peternak_id)->get();
//            dd($data);

            $tgl_start_date = new Carbon($start_date);
            $tgl_end_date = new Carbon($end_date);


            $all_dates = array();
            while ($tgl_start_date->lte($tgl_end_date)){
                $all_dates[] = $tgl_start_date->toDateString();

                $tgl_start_date->addDay();
            }

        } else{
            $data = Deteksi::where('peternakan_id',$peternak_id)->get();

        }
        $tmp = [];
        $k = 0;
        foreach ($all_dates as $tgl){
//            echo $tgl.'<br>';
            $x =0;
            $as= [];

            $biru = 0;
            $putih = 0;

            $biruPagi = 0;
            $putihPagi = 0;
            $totalPagi = 0;

            $biruSore = 0;
            $putihSore = 0;
            $totalSore = 0;

            foreach ($data as $a){
                $time = Carbon::parse($a->time)->toDateString();
                $timeDetail = Carbon::parse($a->time)->format('H');
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';

                if ($timeDetail >= '07' && $timeDetail <= '13'){
                    if ($time == $tgl){
                        if ($a->warna == "Biru"){
                            $biruPagi++;
                        }else{
                            $putihPagi++;
                        }
                        $totalPagi++;
                    }
                }


                if ($timeDetail >= '16' && $timeDetail <= '21'){
                    if ($time == $tgl){
                        if ($a->warna == "Biru"){
                            $biruSore++;
                        }else{
                            $putihSore++;
                        }
                        $totalSore++;
                    }
                }

//                dd($time);
                if ($time == $tgl){
                    if ($a->warna == "Biru"){
                        $biru++;
                    }else{
                        $putih++;
                    }
                    $as[$x]= $a->warna. ' ' . $timeDetail;
                    $x++;
                }
            }

            $warnaPagi = '';
            $scorePagi = 0;
            if ($biruPagi >= 6 ){
                $warnaPagi = 'bagus';
                $scorePagi = 3;
            }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                $warnaPagi = 'sedang';
                $scorePagi = 2;
            }else{
                $warnaPagi = 'buruk';
                $scorePagi = 1;
            }

            $warnaSore = '';
            $scoreSore = 0 ;
            if ($biruSore >= 6 ){
                $warnaSore = 'bagus';
                $scoreSore= 3;
            }elseif ($biruSore >= 4 && $biruSore <= 5){
                $warnaSore = 'sedang';
                $scoreSore = 2;
            }else{
                $warnaSore = 'buruk';
                $scoreSore = 1;
            }

            $rataSocre = ($scorePagi + $scoreSore)/2;


            $hargaFix = '';
            if ( ceil($rataSocre) == 3 ){
                $hargaFix = 6350;
            }elseif (ceil($rataSocre) == 2){
                $hargaFix = 6250;

            }elseif (ceil($rataSocre) == 1){
                $hargaFix = 6150;

            }
            $warna = '';
            if ($biru >= 6 ){
                $warna = 'bagus';
            }elseif ($biru >= 4 && $biru <= 5){
                $warna = 'sedang';
            }else{
                $warna = 'buruk';
            }





            $tmp[$k] = [
                'tgl' =>$tgl,
//                    'warna' => $warna,
                'data' => $as,
                'total_pagi' => $totalPagi,
                'total_sore' => $totalSore,
                'pagi' => ($totalPagi!=0) ?$warnaPagi : null,
                'sore' => ($totalSore!=0) ?$warnaSore : null,
                'totalScore' => ceil($rataSocre),
                'harga' => $hargaFix
            ];
            $k++;
        }


            if($request->ajax()) {
            $data = KualitasPeternak::where('peternakan_id',$peternak_id)->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('Kualitas_Pagi', function($data){
                    $sa = Deteksi::where('peternakan_id',$data->peternakan_id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                    $biruPagi = 0;
                    foreach ($sa as $a){
                        $timeDetail = Carbon::parse($a->time)->format('H');

                        if ($timeDetail >= '07' && $timeDetail <= '13'){
                            if ($a->warna == "Biru"){
                                $biruPagi++;
                            }
                        }

                    }

                    if ($biruPagi >= 6 ){
                        $warnaPagi = 'bagus';
                        $scorePagi = 3;
                    }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                        $warnaPagi = 'sedang';
                        $scorePagi = 2;
                    }elseif ($biruPagi == 0){
                        $warnaPagi = null;
                        $scorePagi = 0;
                    } else{
                        $warnaPagi = 'buruk';
                        $scorePagi = 1;
                    }
//

//                        if ($timeDetail >= '16' && $timeDetail <= '21'){
//
//                        }
                    return $warnaPagi;
                })
                ->addColumn('Kualitas_Sore', function($data){
                    $sa = Deteksi::where('peternakan_id',$data->peternakan_id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                    $biruSore = 0;

                    foreach ($sa as $a){
                        $timeDetail = Carbon::parse($a->time)->format('H');

                        if ($timeDetail >= '16' && $timeDetail <= '21'){
                            if ($a->warna == "Biru"){
                                $biruSore++;
                            }
                        }

                    }

                    if ($biruSore >= 6 ){
                        $warnaSore = 'bagus';
                        $scoreSore = 3;
                    }elseif ($biruSore >= 4 && $biruSore <= 5){
                        $warnaSore = 'sedang';
                        $scoreSore = 2;
                    }elseif ($biruSore == 0){
                        $warnaSore = null;
                        $scoreSore = 0;
                    }else{
                        $warnaSore = 'buruk';
                        $scoreSore = 1;
                    }

                    return $warnaSore;

                })
                ->addColumn('Liter_Total', function($data){
                    $a = $data->Liter_Pagi + $data->Liter_Sore;
                    return '<div class="row p-1">' . $a . '</div>';
                })
                ->addColumn('Kualitas_Total', function($data){
                    $sa = Deteksi::where('peternakan_id',$data->peternakan_id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                    $biruPagi = 0;
                    $biruSore = 0;
                    foreach ($sa as $a){
                        $timeDetail = Carbon::parse($a->time)->format('H');

                        if ($timeDetail >= '07' && $timeDetail <= '13'){
                            if ($a->warna == "Biru"){
                                $biruPagi++;
                            }
                        }
                        if ($timeDetail >= '16' && $timeDetail <= '21'){
                            if ($a->warna == "Biru"){
                                $biruSore++;
                            }
                        }


                    }

                    if ($biruPagi >= 6 ){
                        $warnaPagi = 'bagus';
                        $scorePagi = 3;
                    }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                        $warnaPagi = 'sedang';
                        $scorePagi = 2;
                    }elseif ($biruPagi == 0){
                        $warnaPagi = null;
                        $scorePagi = 0;
                    } else{
                        $warnaPagi = 'buruk';
                        $scorePagi = 1;
                    }


                    if ($biruSore >= 6 ){
                        $warnaSore = 'bagus';
                        $scoreSore = 3;
                    }elseif ($biruSore >= 4 && $biruSore <= 5){
                        $warnaSore = 'sedang';
                        $scoreSore = 2;
                    }elseif ($biruSore == 0){
                        $warnaSore = null;
                        $scoreSore = 0;
                    }else{
                        $warnaSore = 'buruk';
                        $scoreSore = 1;
                    }

                    $rataSocre  =ceil (($scorePagi +$scoreSore) /2);

                    $warnaFix = '';
                    if ( ceil($rataSocre) == 3 ){
                        $warnaFix = 'bagus';
                    }elseif (ceil($rataSocre) == 2){
                        $warnaFix = 'sedang';

                    }elseif (ceil($rataSocre) == 1){
                        $warnaFix = 'buruk';

                    }
                    return '<div class="row p-1">' .$warnaFix. '</div>';
                })->addColumn('total_harga', function($data){
                    $sa = Deteksi::where('peternakan_id',$data->peternakan_id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                    $biruPagi = 0;
                    $biruSore = 0;
                    foreach ($sa as $a){
                        $timeDetail = Carbon::parse($a->time)->format('H');

                        if ($timeDetail >= '07' && $timeDetail <= '13'){
                            if ($a->warna == "Biru"){
                                $biruPagi++;
                            }
                        }
                        if ($timeDetail >= '16' && $timeDetail <= '21'){
                            if ($a->warna == "Biru"){
                                $biruSore++;
                            }
                        }


                    }

                    if ($biruPagi >= 6 ){
                        $warnaPagi = 'bagus';
                        $scorePagi = 3;
                    }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                        $warnaPagi = 'sedang';
                        $scorePagi = 2;
                    }elseif ($biruPagi == 0){
                        $warnaPagi = null;
                        $scorePagi = 0;
                    } else{
                        $warnaPagi = 'buruk';
                        $scorePagi = 1;
                    }


                    if ($biruSore >= 6 ){
                        $warnaSore = 'bagus';
                        $scoreSore = 3;
                    }elseif ($biruSore >= 4 && $biruSore <= 5){
                        $warnaSore = 'sedang';
                        $scoreSore = 2;
                    }elseif ($biruSore == 0){
                        $warnaSore = null;
                        $scoreSore = 0;
                    }else{
                        $warnaSore = 'buruk';
                        $scoreSore = 1;
                    }

                    $rataSocre  =ceil (($scorePagi +$scoreSore) /2);

                    $a = $data->Liter_Pagi + $data->Liter_Sore;

                    $harga = 0;
                    $warnaFix = '';
                    if ( ceil($rataSocre) == 3 ){
                        $warnaFix = 'bagus';
                        $harga = $a * 6350;
                    }elseif (ceil($rataSocre) == 2){
                        $warnaFix = 'sedang';
                        $harga = $a * 6250;
                    }elseif (ceil($rataSocre) == 1){
                        $warnaFix = 'buruk';
                        $harga = $a * 6150;
                    }

                    $hasil_rupiah = "Rp " . number_format($harga,2,',','.');

                    return '<div class="row p-1">' .$hasil_rupiah. '</div>';
                })
                ->rawColumns(['Kualitas_Pagi','Kualitas_sore','Liter_Total','Kualitas_Total','total_harga'])
                ->make(true);
        }

//        $datas = $this->peternak();

//            dd($tmp);
        view()->share([
                'data' => $tmp,
                'peternak_id' => $peternak_id,
                'star_date' => $request->start_date,
                'end_date' => $request->end_date,
            ]
        );
        return view('admin.content.dashboard.peternak');

    }


    public function index(Request $request)
    {
//        dd('ss');
        if (auth()->user()->level == 'peternak'){
//            dd('xxx');
            $all_dates = [
                '2022-12-08'
            ];

            if ($request->start_date && $request->end_date){
                $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
                $end_date = Carbon::parse(request()->end_date)->toDateTimeString();

                $data = Deteksi::where(function ($query) {
                    $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
                    $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
                    $query->whereDate('time', '>=', $start_date)
                        ->whereDate('time', '<=', $end_date);
                })->where('peternakan_id',auth()->user()->peternakan->id)->get();
//            dd($data);

                $tgl_start_date = new Carbon($start_date);
                $tgl_end_date = new Carbon($end_date);


                $all_dates = array();
                while ($tgl_start_date->lte($tgl_end_date)){
                    $all_dates[] = $tgl_start_date->toDateString();

                    $tgl_start_date->addDay();
                }



//            dd($tmp);
            } else{
                $data = Deteksi::where('peternakan_id',auth()->user()->peternakan->id)->get();

            }


            $tmp = [];
            $k = 0;
            foreach ($all_dates as $tgl){
//            echo $tgl.'<br>';
                $x =0;
                $as= [];

                $biru = 0;
                $putih = 0;

                $biruPagi = 0;
                $putihPagi = 0;
                $totalPagi = 0;

                $biruSore = 0;
                $putihSore = 0;
                $totalSore = 0;

//                dd($data)
                foreach ($data as $a){
                    $time = Carbon::parse($a->time)->toDateString();
                    $timeDetail = Carbon::parse($a->time)->format('H');
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';

                    if ($timeDetail >= '07' && $timeDetail <= '13'){
                        if ($time == $tgl){
                            if ($a->warna == "Biru"){
                                $biruPagi++;
                            }else{
                                $putihPagi++;
                            }
                            $totalPagi++;
                        }
                    }


                    if ($timeDetail >= '16' && $timeDetail <= '21'){
                        if ($time == $tgl){
                            if ($a->warna == "Biru"){
                                $biruSore++;
                            }else{
                                $putihSore++;
                            }
                            $totalSore++;
                        }
                    }

//                dd($time);
                    if ($time == $tgl){
                        if ($a->warna == "Biru"){
                            $biru++;
                        }else{
                            $putih++;
                        }
                        $as[$x]= $a->warna. ' ' . $timeDetail;
                        $x++;
                    }
                }

                $warnaPagi = '';
                $scorePagi = 0;
                if ($biruPagi >= 6 ){
                    $warnaPagi = 'bagus';
                    $scorePagi = 3;
                }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                    $warnaPagi = 'sedang';
                    $scorePagi = 2;
                }else{
                    $warnaPagi = 'buruk';
                    $scorePagi = 1;
                }

                $warnaSore = '';
                $scoreSore = 0 ;
                if ($biruSore >= 6 ){
                    $warnaSore = 'bagus';
                    $scoreSore= 3;
                }elseif ($biruSore >= 4 && $biruSore <= 5){
                    $warnaSore = 'sedang';
                    $scoreSore = 2;
                }else{
                    $warnaSore = 'buruk';
                    $scoreSore = 1;
                }

                $rataSocre = ($scorePagi + $scoreSore)/2;


                $hargaFix = '';
                if ( ceil($rataSocre) == 3 ){
                    $hargaFix = 6350;
                }elseif (ceil($rataSocre) == 2){
                    $hargaFix = 6250;

                }elseif (ceil($rataSocre) == 1){
                    $hargaFix = 6150;

                }
                $warna = '';
                if ($biru >= 6 ){
                    $warna = 'bagus';
                }elseif ($biru >= 4 && $biru <= 5){
                    $warna = 'sedang';
                }else{
                    $warna = 'buruk';
                }






                $tmp[$k] = [
                    'tgl' =>$tgl,
//                    'warna' => $warna,
                    'data' => $as,
                    'total_pagi' => $totalPagi,
                    'total_sore' => $totalSore,
                    'pagi' => ($totalPagi!=0) ?$warnaPagi : null,
                    'sore' => ($totalSore!=0) ?$warnaSore : null,
                    'totalScore' => ceil($rataSocre),
                    'harga' => $hargaFix
                ];
                $k++;
            }

            $total_harga = 0;

            foreach ($tmp as $sx){
                $total_harga =$total_harga + $sx['harga'];
            }
//            dd($total_harga);


            if($request->ajax()) {
                $data = KualitasPeternak::where('peternakan_id',auth()->user()->peternakan->id)->get();

                return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('Kualitas_Pagi', function($data){
                        $sa = Deteksi::where('peternakan_id',auth()->user()->peternakan->id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                        $biruPagi = 0;

                        foreach ($sa as $a){
                            $timeDetail = Carbon::parse($a->time)->format('H');

                            if ($timeDetail >= '07' && $timeDetail <= '13'){
                                if ($a->warna == "Biru"){
                                    $biruPagi++;
                                }
                            }

                        }
//                        $warnaPagi = 'buruk';
//                        return $biruPagi;
                        if ($biruPagi >= 6 ){
                            $warnaPagi = 'bagus';
                            $scorePagi = 3;
                        }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                            $warnaPagi = 'sedang';
                            $scorePagi = 2;
                        }elseif ($biruPagi == 0){
                            $warnaPagi = 'buruk';
                            $scorePagi = 0;
                        } else{
                            $warnaPagi = 'buruk';
                            $scorePagi = 1;
                        }
//

//                        if ($timeDetail >= '16' && $timeDetail <= '21'){
//
//                        }
                        return $warnaPagi;
                    })->addColumn('Kualitas_Sore', function($data){
                        $sa = Deteksi::where('peternakan_id',auth()->user()->peternakan->id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                        $biruSore = 0;

                        foreach ($sa as $a){
                            $timeDetail = Carbon::parse($a->time)->format('H');

                            if ($timeDetail >= '16' && $timeDetail <= '21'){
                                if ($a->warna == "Biru"){
                                    $biruSore++;
                                }
                            }

                        }

                        if ($biruSore >= 6 ){
                            $warnaSore = 'bagus';
                            $scoreSore = 3;
                        }elseif ($biruSore >= 4 && $biruSore <= 5){
                            $warnaSore = 'sedang';
                            $scoreSore = 2;
                        }elseif ($biruSore == 0){
                            $warnaSore = 'buruk';
                            $scoreSore = 0;
                        }else{
                            $warnaSore = 'buruk';
                            $scoreSore = 1;
                        }

                        return $warnaSore;

                    })
                    ->addColumn('Liter_Total', function($data){
                        $a = $data->Liter_Pagi + $data->Liter_Sore;
                        return '<div class="row p-1">' . $a . '</div>';
                    })->addColumn('Kualitas_Total', function($data){
                        $sa = Deteksi::where('peternakan_id',auth()->user()->peternakan->id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                        $biruPagi = 0;
                        $biruSore = 0;
                        foreach ($sa as $a){
                            $timeDetail = Carbon::parse($a->time)->format('H');

                            if ($timeDetail >= '07' && $timeDetail <= '13'){
                                if ($a->warna == "Biru"){
                                    $biruPagi++;
                                }
                            }
                            if ($timeDetail >= '16' && $timeDetail <= '21'){
                                if ($a->warna == "Biru"){
                                    $biruSore++;
                                }
                            }


                        }

                        if ($biruPagi >= 6 ){
                            $warnaPagi = 'bagus';
                            $scorePagi = 3;
                        }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                            $warnaPagi = 'sedang';
                            $scorePagi = 2;
                        }elseif ($biruPagi == 0){
                            $warnaPagi = null;
                            $scorePagi = 0;
                        } else{
                            $warnaPagi = 'buruk';
                            $scorePagi = 1;
                        }


                        if ($biruSore >= 6 ){
                            $warnaSore = 'bagus';
                            $scoreSore = 3;
                        }elseif ($biruSore >= 4 && $biruSore <= 5){
                            $warnaSore = 'sedang';
                            $scoreSore = 2;
                        }elseif ($biruSore == 0){
                            $warnaSore = null;
                            $scoreSore = 0;
                        }else{
                            $warnaSore = 'buruk';
                            $scoreSore = 1;
                        }

                        $rataSocre  =ceil (($scorePagi +$scoreSore) /2);

                        $warnaFix = '';
                        if ( ceil($rataSocre) == 3 ){
                            $warnaFix = 'bagus';
                        }elseif (ceil($rataSocre) == 2){
                            $warnaFix = 'sedang';

                        }elseif (ceil($rataSocre) == 1){
                            $warnaFix = 'buruk';

                        }
                        return '<div class="row p-1">' .$warnaFix. '</div>';
                    })
                    ->rawColumns(['Kualitas_Pagi','Kualitas_sore','Liter_Total','Kualitas_Total'])
                    ->make(true);
            }

//            $datas = $this->peternak();

//            dd($tmp);
            view()->share([
                    'data' => $tmp,
                    'total_harga' => $total_harga,
                    'start_date' => $request->start_date,
                    'end_date' => $request->end_date,
                ]
            );
            return view('admin.content.dashboard.index');

        }
        else{
//            dd('ssss');
            $all_dates = array();

            $jumlah_peternak = null;

            if ($request->start_date && $request->end_date){

                $peternak = Peternakan::all();

                $jumlah_peternak = count($peternak);
                $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
                $end_date = Carbon::parse(request()->end_date)->toDateTimeString();


                $tmp = [];
                foreach ($peternak as $a){

                    $id_ternak = $a->id;
                    $data = Deteksi::where(function ($query) use ($id_ternak) {
                        $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
                        $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
                        $query->whereDate('time', '>=', $start_date)
                            ->whereDate('time', '<=', $end_date)
                            ->where('peternakan_id',$id_ternak);
                    })->get();

//                    dd($data);
                    $tmp[$a->id] = $data;

                }
//                dd($tmp);

                $fixQualiti = [];
                foreach ($tmp as $sa){
//                    dd($sa);
                    $biruPagi = 1;
                    $putihPagi = 1;

                    $biruSore = 1;
                    $putihSore = 1;

                    foreach ($sa as $a){
                        $time = Carbon::parse($a->time)->toDateString();
                        $timeDetail = Carbon::parse($a->time)->format('H');
//                        dd($timeDetail);
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';

                        if ($timeDetail >= '07' && $timeDetail <= '13'){
                            if ($a->warna == "Biru"){
                                $biruPagi++;
                            }else{
                                $putihPagi++;
                            }
//                            $totalPagi++;
//                            dd('sini');
                        }


                        if ($timeDetail >= '16' && $timeDetail <= '21'){
                            if ($a->warna == "Biru"){
                                $biruSore++;
                            }else{
                                $putihSore++;
                            }
//                            $totalSore++;
                        }
                    }

                    $pagi = null;

                    $pagee = $biruPagi- $putihPagi;
                    $soree = $biruSore- $putihSore;
//                    dd($soree);
                    $scorePagi = 0;
                    $scoreSore = 0;
//                    dd($biruPagi- $putihPagi);
                    if ($pagee >= 6 ){
                        $scorePagi = 3;
                    }elseif ($pagee >= 4 && $pagee <= 5){
                        $scorePagi = 2;
                    }elseif ($pagee == 0){
                        $scorePagi = 0;
                    } else{
                        $scorePagi = 1;
                    }

                    if ($soree >= 6 ){
                        $scoreSore = 3;
                    }elseif ($soree >= 4 && $soree <= 5){
                        $scoreSore = 2;
                    }elseif ($soree == 0){
                        $scoreSore = 0;
                    } else{
                        $scoreSore = 1;
                    }

                    $tot = ceil(($scorePagi +$scoreSore)/2);
//                    $rataSocre  =ceil (($scorePagi +$scoreSore) /2);

                    $warnaFixx = '';
                    if ( ceil($tot) == 3 ){
                        $warnaFixx = 'bagus';
                    }elseif (ceil($tot) == 2){
                        $warnaFixx = 'sedang';

                    }elseif (ceil($tot) == 1){
                        $warnaFixx = 'buruk';

                    }

                    $fixQualiti[] = [
                        $warnaFixx,
                        $sa[0]->peternakan_id
                    ];
                }

                $buruk = 0;
                $sedang = 0;
                $baik = 0;
                $baikTernak = [];
                $sedangTernak = [];
                $burukTernak = [];

//                dd($fixQualiti);
                $oi = 0;

                foreach ($fixQualiti as $sx){

                    if ($sx[0] == 'buruk'){
                        $buruk++;
                        $burukTernak[] = $sx[1];
                    }

                    if ($sx[0] == 'sedang'){
                        $sedang++;
                        $sedangTernak[] = $sx[1];

                    }

                    if ($sx[0] == 'baik'){
                        $baik++;
                        $baikTernak[] = $sx[1];

                    }
                }

                $kmean_baik = [];
                $kmean_sedang = [];
                $kmean_buruk = [];

                foreach ($baikTernak as $zx){
//                    dd($zx);
                    $tenah = Peternakan::find($zx);
                    $luas = inputPeternak::where('peternakan_id',$tenah->id)->first();
                    $peternakan = Peternakan::find($tenah->id);
                    $itung_jumlah_ideal_sapi = floor(($luas->luas_peternakan/10));
                    $tambah_setengah_ideal = ($itung_jumlah_ideal_sapi/2) + $itung_jumlah_ideal_sapi;
                    $tambah_seperempat_ideal = ($itung_jumlah_ideal_sapi*0.25) + $itung_jumlah_ideal_sapi;
                    $tambah_tigaperempat_ideal = ($itung_jumlah_ideal_sapi*0.75) + $itung_jumlah_ideal_sapi;
                    $duakalilipat = $itung_jumlah_ideal_sapi*2;

                    $index_stess_baik =0;
                    if ($luas->jumlah_sapi<=$itung_jumlah_ideal_sapi){
                        $index_stess_baik = 1;
                    }elseif ($luas->jumlah_sapi >= $tambah_setengah_ideal){
                        $index_stess_baik = 3;
                    }elseif ($luas->jumlah_sapi >= $duakalilipat){
                        $index_stess_baik = 5;
                    }elseif ($luas->jumlah_sapi <= $tambah_seperempat_ideal){
                        $index_stess_baik = 2;
                    }elseif ($luas->jumlah_sapi <= $tambah_tigaperempat_ideal){
                        $index_stess_baik = 4;
                    }
                    $kmean_baik[] = [
                        $peternakan->nama_peternakan,$index_stess_baik];

//                    dd($itung_jumlah_ideal_sapi);
//                    dd($luas);
//                    dd($tenah);
                }

                foreach ($sedangTernak as $zx){
//                    dd($zx);
                    $tenah = Peternakan::find($zx);
                    $luas = inputPeternak::where('peternakan_id',$tenah->id)->first();
                    $peternakan = Peternakan::find($tenah->id);

                    $itung_jumlah_ideal_sapi = floor(($luas->luas_peternakan/10));
                    $tambah_setengah_ideal = ($itung_jumlah_ideal_sapi/2) + $itung_jumlah_ideal_sapi;
                    $tambah_seperempat_ideal = ($itung_jumlah_ideal_sapi*0.25) + $itung_jumlah_ideal_sapi;
                    $tambah_tigaperempat_ideal = ($itung_jumlah_ideal_sapi*0.75) + $itung_jumlah_ideal_sapi;
                    $duakalilipat = $itung_jumlah_ideal_sapi*2;

                    $index_stess_sedang =0;
                    if ($luas->jumlah_sapi<=$itung_jumlah_ideal_sapi){
                        $index_stess_sedang = 1;
                    }elseif ($luas->jumlah_sapi >= $tambah_setengah_ideal){
                        $index_stess_sedang = 3;
                    }elseif ($luas->jumlah_sapi >= $duakalilipat){
                        $index_stess_sedang = 5;
                    }elseif ($luas->jumlah_sapi <= $tambah_seperempat_ideal){
                        $index_stess_sedang = 2;
                    }elseif ($luas->jumlah_sapi <= $tambah_tigaperempat_ideal){
                        $index_stess_sedang = 4;
                    }
                    $kmean_sedang[] = [
                        $peternakan->nama_peternakan,$index_stess_sedang];

//                    dd($itung_jumlah_ideal_sapi);
//                    dd($luas);
//                    dd($tenah);
                }

                foreach ($burukTernak as $zx){
//                    dd($zx);
                    $tenah = Peternakan::find($zx);
                    $luas = inputPeternak::where('peternakan_id',$tenah->id)->first();
                    $peternakan = Peternakan::find($tenah->id);

                    $itung_jumlah_ideal_sapi = floor(($luas->luas_peternakan/10));
                    $tambah_setengah_ideal = ($itung_jumlah_ideal_sapi/2) + $itung_jumlah_ideal_sapi;
                    $tambah_seperempat_ideal = ($itung_jumlah_ideal_sapi*0.25) + $itung_jumlah_ideal_sapi;
                    $tambah_tigaperempat_ideal = ($itung_jumlah_ideal_sapi*0.75) + $itung_jumlah_ideal_sapi;
                    $duakalilipat = $itung_jumlah_ideal_sapi*2;

                    $index_stess =0;
                    if ($luas->jumlah_sapi<=$itung_jumlah_ideal_sapi){
                        $index_stess = 1;
                    }elseif ($luas->jumlah_sapi >= $tambah_setengah_ideal){
                        $index_stess = 3;
                    }elseif ($luas->jumlah_sapi >= $duakalilipat){
                        $index_stess = 5;
                    }elseif ($luas->jumlah_sapi <= $tambah_seperempat_ideal){
                        $index_stess = 2;
                    }elseif ($luas->jumlah_sapi <= $tambah_tigaperempat_ideal){
                        $index_stess = 4;
                    }
                    $kmean_buruk[] = [
                        $peternakan->nama_peternakan,$index_stess];

//                    dd($itung_jumlah_ideal_sapi);
//                    dd($luas);
//                    dd($tenah);
                }
//                dd($kmean);

//                dd($kmean_sedang);

            } else{
                $data = Deteksi::limit(5)->get();

                $kmean_baik = [];
                $kmean_sedang = [];
                $kmean_buruk = [];
                $buruk = 0;
                $sedang = 0;
                $baik = 0;
            }
//            dd($buruk);

//            dd($kmean_buruk);

            if($request->ajax()) {
                $data = KualitasPeternak::with('peternakan');

                return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('name', function($data){

                        $a = '<a href="'.route('peternak',$data->peternakan_id).'">'.$data->peternakan->code_peternak.'</a>';
                        return $a;
                    })
                    ->addColumn('Kualitas_Pagi', function($data){
                        $sa = Deteksi::where('peternakan_id',$data->peternakan_id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                        $biruPagi = 0;
                        foreach ($sa as $a){
                            $timeDetail = Carbon::parse($a->time)->format('H');

                            if ($timeDetail >= '07' && $timeDetail <= '13'){
                                if ($a->warna == "Biru"){
                                    $biruPagi++;
                                }
                            }

                        }

                        if ($biruPagi >= 6 ){
                            $warnaPagi = 'bagus';
                            $scorePagi = 3;
                        }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                            $warnaPagi = 'sedang';
                            $scorePagi = 2;
                        }elseif ($biruPagi == 0){
                            $warnaPagi = null;
                            $scorePagi = 0;
                        } else{
                            $warnaPagi = 'buruk';
                            $scorePagi = 1;
                        }
//

//                        if ($timeDetail >= '16' && $timeDetail <= '21'){
//
//                        }
                        return $warnaPagi;
                    })
                    ->addColumn('Kualitas_Sore', function($data){
                        $sa = Deteksi::where('peternakan_id',$data->peternakan_id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                        $biruSore   = 0;

                        foreach ($sa as $a){
                            $timeDetail = Carbon::parse($a->time)->format('H');

                            if ($timeDetail >= '16' && $timeDetail <= '21'){
                                if ($a->warna == "Biru"){
                                    $biruSore++;
                                }
                            }

                        }

                        if ($biruSore >= 6 ){
                            $warnaSore = 'bagus';
                            $scoreSore = 3;
                        }elseif ($biruSore >= 4 && $biruSore <= 5){
                            $warnaSore = 'sedang';
                            $scoreSore = 2;
                        }elseif ($biruSore == 0){
                            $warnaSore = null;
                            $scoreSore = 0;
                        }else{
                            $warnaSore = 'buruk';
                            $scoreSore = 1;
                        }

                        return $warnaSore;

                    })
                    ->addColumn('Liter_Total', function($data){
                        $a = $data->Liter_Pagi + $data->Liter_Sore;
                        return '<div class="row p-1">' . $a . '</div>';
                    })
                    ->addColumn('Kualitas_Total', function($data){
                        $sa = Deteksi::where('peternakan_id',$data->peternakan_id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                        $biruPagi = 0;
                        $biruSore = 0;
                        foreach ($sa as $a){
                            $timeDetail = Carbon::parse($a->time)->format('H');

                            if ($timeDetail >= '07' && $timeDetail <= '13'){
                                if ($a->warna == "Biru"){
                                    $biruPagi++;
                                }
                            }
                            if ($timeDetail >= '16' && $timeDetail <= '21'){
                                if ($a->warna == "Biru"){
                                    $biruSore++;
                                }
                            }


                        }

                        if ($biruPagi >= 6 ){
                            $warnaPagi = 'bagus';
                            $scorePagi = 3;
                        }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                            $warnaPagi = 'sedang';
                            $scorePagi = 2;
                        }elseif ($biruPagi == 0){
                            $warnaPagi = null;
                            $scorePagi = 0;
                        } else{
                            $warnaPagi = 'buruk';
                            $scorePagi = 1;
                        }


                        if ($biruSore >= 6 ){
                            $warnaSore = 'bagus';
                            $scoreSore = 3;
                        }elseif ($biruSore >= 4 && $biruSore <= 5){
                            $warnaSore = 'sedang';
                            $scoreSore = 2;
                        }elseif ($biruSore == 0){
                            $warnaSore = null;
                            $scoreSore = 0;
                        }else{
                            $warnaSore = 'buruk';
                            $scoreSore = 1;
                        }

                        $rataSocre  =ceil (($scorePagi +$scoreSore) /2);

                        $warnaFix = '';
                        if ( ceil($rataSocre) == 3 ){
                            $warnaFix = 'bagus';
                        }elseif (ceil($rataSocre) == 2){
                            $warnaFix = 'sedang';

                        }elseif (ceil($rataSocre) == 1){
                            $warnaFix = 'buruk';

                        }
                        return '<div class="row p-1">' .$warnaFix. '</div>';
                    })
                    ->rawColumns(['name','Kualitas_Pagi','Kualitas_sore','Liter_Total','Kualitas_Total'])
                    ->make(true);
            }

            view()->share([
                'buruk' => $buruk,
                'sedang' => $sedang,
                'baik' => $baik,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'kmean_baik' => $kmean_baik,
                'kmean_sedang' => $kmean_sedang,
                'kmean_buruk' => $kmean_buruk,
            ]);

            return view('admin.content.dashboard.admin');
        }
    }
    public function kualitas(Request $request)
    {
//        dd('ss');
        if (auth()->user()->level == 'peternak'){
//            dd('xxx');
            $all_dates = [
                '2022-12-08'
            ];

            if ($request->start_date && $request->end_date){
                $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
                $end_date = Carbon::parse(request()->end_date)->toDateTimeString();

                $data = Deteksi::where(function ($query) {
                    $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
                    $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
                    $query->whereDate('time', '>=', $start_date)
                        ->whereDate('time', '<=', $end_date);
                })->where('peternakan_id',auth()->user()->peternakan->id)->get();
//            dd($data);

                $tgl_start_date = new Carbon($start_date);
                $tgl_end_date = new Carbon($end_date);


                $all_dates = array();
                while ($tgl_start_date->lte($tgl_end_date)){
                    $all_dates[] = $tgl_start_date->toDateString();

                    $tgl_start_date->addDay();
                }



//            dd($tmp);
            } else{
                $data = Deteksi::where('peternakan_id',auth()->user()->peternakan->id)->get();

            }


            $tmp = [];
            $k = 0;
            foreach ($all_dates as $tgl){
//            echo $tgl.'<br>';
                $x =0;
                $as= [];

                $biru = 0;
                $putih = 0;

                $biruPagi = 0;
                $putihPagi = 0;
                $totalPagi = 0;

                $biruSore = 0;
                $putihSore = 0;
                $totalSore = 0;

//                dd($data)
                foreach ($data as $a){
                    $time = Carbon::parse($a->time)->toDateString();
                    $timeDetail = Carbon::parse($a->time)->format('H');
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';

                    if ($timeDetail >= '07' && $timeDetail <= '13'){
                        if ($time == $tgl){
                            if ($a->warna == "Biru"){
                                $biruPagi++;
                            }else{
                                $putihPagi++;
                            }
                            $totalPagi++;
                        }
                    }


                    if ($timeDetail >= '16' && $timeDetail <= '21'){
                        if ($time == $tgl){
                            if ($a->warna == "Biru"){
                                $biruSore++;
                            }else{
                                $putihSore++;
                            }
                            $totalSore++;
                        }
                    }

//                dd($time);
                    if ($time == $tgl){
                        if ($a->warna == "Biru"){
                            $biru++;
                        }else{
                            $putih++;
                        }
                        $as[$x]= $a->warna. ' ' . $timeDetail;
                        $x++;
                    }
                }

                $warnaPagi = '';
                $scorePagi = 0;
                if ($biruPagi >= 6 ){
                    $warnaPagi = 'bagus';
                    $scorePagi = 3;
                }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                    $warnaPagi = 'sedang';
                    $scorePagi = 2;
                }else{
                    $warnaPagi = 'buruk';
                    $scorePagi = 1;
                }

                $warnaSore = '';
                $scoreSore = 0 ;
                if ($biruSore >= 6 ){
                    $warnaSore = 'bagus';
                    $scoreSore= 3;
                }elseif ($biruSore >= 4 && $biruSore <= 5){
                    $warnaSore = 'sedang';
                    $scoreSore = 2;
                }else{
                    $warnaSore = 'buruk';
                    $scoreSore = 1;
                }

                $rataSocre = ($scorePagi + $scoreSore)/2;


                $hargaFix = '';
                if ( ceil($rataSocre) == 3 ){
                    $hargaFix = 6350;
                }elseif (ceil($rataSocre) == 2){
                    $hargaFix = 6250;

                }elseif (ceil($rataSocre) == 1){
                    $hargaFix = 6150;

                }
                $warna = '';
                if ($biru >= 6 ){
                    $warna = 'bagus';
                }elseif ($biru >= 4 && $biru <= 5){
                    $warna = 'sedang';
                }else{
                    $warna = 'buruk';
                }






                $tmp[$k] = [
                    'tgl' =>$tgl,
//                    'warna' => $warna,
                    'data' => $as,
                    'total_pagi' => $totalPagi,
                    'total_sore' => $totalSore,
                    'pagi' => ($totalPagi!=0) ?$warnaPagi : null,
                    'sore' => ($totalSore!=0) ?$warnaSore : null,
                    'totalScore' => ceil($rataSocre),
                    'harga' => $hargaFix
                ];
                $k++;
            }

            $total_harga = 0;

            foreach ($tmp as $sx){
                $total_harga =$total_harga + $sx['harga'];
            }
//            dd($total_harga);


            if($request->ajax()) {
                $data = KualitasPeternak::where('peternakan_id',auth()->user()->peternakan->id)->get();

                return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('Kualitas_Pagi', function($data){
                        $sa = Deteksi::where('peternakan_id',auth()->user()->peternakan->id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                        $biruPagi = 0;

                        foreach ($sa as $a){
                            $timeDetail = Carbon::parse($a->time)->format('H');

                            if ($timeDetail >= '07' && $timeDetail <= '13'){
                                if ($a->warna == "Biru"){
                                    $biruPagi++;
                                }
                            }

                        }
//                        $warnaPagi = 'buruk';
//                        return $biruPagi;
                        if ($biruPagi >= 6 ){
                            $warnaPagi = 'bagus';
                            $scorePagi = 3;
                        }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                            $warnaPagi = 'sedang';
                            $scorePagi = 2;
                        }elseif ($biruPagi == 0){
                            $warnaPagi = 'buruk';
                            $scorePagi = 0;
                        } else{
                            $warnaPagi = 'buruk';
                            $scorePagi = 1;
                        }
//

//                        if ($timeDetail >= '16' && $timeDetail <= '21'){
//
//                        }
                        return $warnaPagi;
                    })->addColumn('Kualitas_Sore', function($data){
                        $sa = Deteksi::where('peternakan_id',auth()->user()->peternakan->id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                        $biruSore = 0;

                        foreach ($sa as $a){
                            $timeDetail = Carbon::parse($a->time)->format('H');

                            if ($timeDetail >= '16' && $timeDetail <= '21'){
                                if ($a->warna == "Biru"){
                                    $biruSore++;
                                }
                            }

                        }

                        if ($biruSore >= 6 ){
                            $warnaSore = 'bagus';
                            $scoreSore = 3;
                        }elseif ($biruSore >= 4 && $biruSore <= 5){
                            $warnaSore = 'sedang';
                            $scoreSore = 2;
                        }elseif ($biruSore == 0){
                            $warnaSore = 'buruk';
                            $scoreSore = 0;
                        }else{
                            $warnaSore = 'buruk';
                            $scoreSore = 1;
                        }

                        return $warnaSore;

                    })
                    ->addColumn('Liter_Total', function($data){
                        $a = $data->Liter_Pagi + $data->Liter_Sore;
                        return '<div class="row p-1">' . $a . '</div>';
                    })->addColumn('Kualitas_Total', function($data){
                        $sa = Deteksi::where('peternakan_id',auth()->user()->peternakan->id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                        $biruPagi = 0;
                        $biruSore = 0;
                        foreach ($sa as $a){
                            $timeDetail = Carbon::parse($a->time)->format('H');

                            if ($timeDetail >= '07' && $timeDetail <= '13'){
                                if ($a->warna == "Biru"){
                                    $biruPagi++;
                                }
                            }
                            if ($timeDetail >= '16' && $timeDetail <= '21'){
                                if ($a->warna == "Biru"){
                                    $biruSore++;
                                }
                            }


                        }

                        if ($biruPagi >= 6 ){
                            $warnaPagi = 'bagus';
                            $scorePagi = 3;
                        }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                            $warnaPagi = 'sedang';
                            $scorePagi = 2;
                        }elseif ($biruPagi == 0){
                            $warnaPagi = null;
                            $scorePagi = 0;
                        } else{
                            $warnaPagi = 'buruk';
                            $scorePagi = 1;
                        }


                        if ($biruSore >= 6 ){
                            $warnaSore = 'bagus';
                            $scoreSore = 3;
                        }elseif ($biruSore >= 4 && $biruSore <= 5){
                            $warnaSore = 'sedang';
                            $scoreSore = 2;
                        }elseif ($biruSore == 0){
                            $warnaSore = null;
                            $scoreSore = 0;
                        }else{
                            $warnaSore = 'buruk';
                            $scoreSore = 1;
                        }

                        $rataSocre  =ceil (($scorePagi +$scoreSore) /2);

                        $warnaFix = '';
                        if ( ceil($rataSocre) == 3 ){
                            $warnaFix = 'bagus';
                        }elseif (ceil($rataSocre) == 2){
                            $warnaFix = 'sedang';

                        }elseif (ceil($rataSocre) == 1){
                            $warnaFix = 'buruk';

                        }
                        return '<div class="row p-1">' .$warnaFix. '</div>';
                    })
                    ->rawColumns(['Kualitas_Pagi','Kualitas_sore','Liter_Total','Kualitas_Total'])
                    ->make(true);
            }

//            $datas = $this->peternak();

//            dd($tmp);
            view()->share([
                    'data' => $tmp,
                    'total_harga' => $total_harga,
                    'start_date' => $request->start_date,
                    'end_date' => $request->end_date,
                ]
            );
            return view('admin.content.dashboard.coba');

        }
        else{
//            dd('ssss');
            $all_dates = array();

            $jumlah_peternak = null;

            if ($request->start_date && $request->end_date){

                $peternak = Peternakan::all();

                $jumlah_peternak = count($peternak);
                $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
                $end_date = Carbon::parse(request()->end_date)->toDateTimeString();


                $tmp = [];
                foreach ($peternak as $a){

                    $id_ternak = $a->id;
                    $data = Deteksi::where(function ($query) use ($id_ternak) {
                        $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
                        $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
                        $query->whereDate('time', '>=', $start_date)
                            ->whereDate('time', '<=', $end_date)
                            ->where('peternakan_id',$id_ternak);
                    })->get();

//                    dd($data);
                    $tmp[$a->id] = $data;

                }
//                dd($tmp);

                $fixQualiti = [];
                foreach ($tmp as $sa){
//                    dd($sa);
                    $biruPagi = 1;
                    $putihPagi = 1;

                    $biruSore = 1;
                    $putihSore = 1;

                    foreach ($sa as $a){
                        $time = Carbon::parse($a->time)->toDateString();
                        $timeDetail = Carbon::parse($a->time)->format('H');
//                        dd($timeDetail);
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';

                        if ($timeDetail >= '07' && $timeDetail <= '13'){
                            if ($a->warna == "Biru"){
                                $biruPagi++;
                            }else{
                                $putihPagi++;
                            }
//                            $totalPagi++;
//                            dd('sini');
                        }


                        if ($timeDetail >= '16' && $timeDetail <= '21'){
                            if ($a->warna == "Biru"){
                                $biruSore++;
                            }else{
                                $putihSore++;
                            }
//                            $totalSore++;
                        }
                    }

                    $pagi = null;

                    $pagee = $biruPagi- $putihPagi;
                    $soree = $biruSore- $putihSore;
//                    dd($soree);
                    $scorePagi = 0;
                    $scoreSore = 0;
//                    dd($biruPagi- $putihPagi);
                    if ($pagee >= 6 ){
                        $scorePagi = 3;
                    }elseif ($pagee >= 4 && $pagee <= 5){
                        $scorePagi = 2;
                    }elseif ($pagee == 0){
                        $scorePagi = 0;
                    } else{
                        $scorePagi = 1;
                    }

                    if ($soree >= 6 ){
                        $scoreSore = 3;
                    }elseif ($soree >= 4 && $soree <= 5){
                        $scoreSore = 2;
                    }elseif ($soree == 0){
                        $scoreSore = 0;
                    } else{
                        $scoreSore = 1;
                    }

                    $tot = ceil(($scorePagi +$scoreSore)/2);
//                    $rataSocre  =ceil (($scorePagi +$scoreSore) /2);

                    $warnaFixx = '';
                    if ( ceil($tot) == 3 ){
                        $warnaFixx = 'bagus';
                    }elseif (ceil($tot) == 2){
                        $warnaFixx = 'sedang';

                    }elseif (ceil($tot) == 1){
                        $warnaFixx = 'buruk';

                    }

                    $fixQualiti[] = [
                        $warnaFixx,
                        $sa[0]->peternakan_id
                    ];
                }

                $buruk = 0;
                $sedang = 0;
                $baik = 0;
                $baikTernak = [];
                $sedangTernak = [];
                $burukTernak = [];

//                dd($fixQualiti);
                $oi = 0;

                foreach ($fixQualiti as $sx){

                    if ($sx[0] == 'buruk'){
                        $buruk++;
                        $burukTernak[] = $sx[1];
                    }

                    if ($sx[0] == 'sedang'){
                        $sedang++;
                        $sedangTernak[] = $sx[1];

                    }

                    if ($sx[0] == 'baik'){
                        $baik++;
                        $baikTernak[] = $sx[1];

                    }
                }

                $kmean_baik = [];
                $kmean_sedang = [];
                $kmean_buruk = [];

                foreach ($baikTernak as $zx){
//                    dd($zx);
                    $tenah = Peternakan::find($zx);
                    $luas = inputPeternak::where('peternakan_id',$tenah->id)->first();
                    $peternakan = Peternakan::find($tenah->id);
                    $itung_jumlah_ideal_sapi = floor(($luas->luas_peternakan/10));
                    $tambah_setengah_ideal = ($itung_jumlah_ideal_sapi/2) + $itung_jumlah_ideal_sapi;
                    $tambah_seperempat_ideal = ($itung_jumlah_ideal_sapi*0.25) + $itung_jumlah_ideal_sapi;
                    $tambah_tigaperempat_ideal = ($itung_jumlah_ideal_sapi*0.75) + $itung_jumlah_ideal_sapi;
                    $duakalilipat = $itung_jumlah_ideal_sapi*2;

                    $index_stess_baik =0;
                    if ($luas->jumlah_sapi<=$itung_jumlah_ideal_sapi){
                        $index_stess_baik = 1;
                    }elseif ($luas->jumlah_sapi >= $tambah_setengah_ideal){
                        $index_stess_baik = 3;
                    }elseif ($luas->jumlah_sapi >= $duakalilipat){
                        $index_stess_baik = 5;
                    }elseif ($luas->jumlah_sapi <= $tambah_seperempat_ideal){
                        $index_stess_baik = 2;
                    }elseif ($luas->jumlah_sapi <= $tambah_tigaperempat_ideal){
                        $index_stess_baik = 4;
                    }
                    $kmean_baik[] = [
                        $peternakan->nama_peternakan,$index_stess_baik];

//                    dd($itung_jumlah_ideal_sapi);
//                    dd($luas);
//                    dd($tenah);
                }

                foreach ($sedangTernak as $zx){
//                    dd($zx);
                    $tenah = Peternakan::find($zx);
                    $luas = inputPeternak::where('peternakan_id',$tenah->id)->first();
                    $peternakan = Peternakan::find($tenah->id);

                    $itung_jumlah_ideal_sapi = floor(($luas->luas_peternakan/10));
                    $tambah_setengah_ideal = ($itung_jumlah_ideal_sapi/2) + $itung_jumlah_ideal_sapi;
                    $tambah_seperempat_ideal = ($itung_jumlah_ideal_sapi*0.25) + $itung_jumlah_ideal_sapi;
                    $tambah_tigaperempat_ideal = ($itung_jumlah_ideal_sapi*0.75) + $itung_jumlah_ideal_sapi;
                    $duakalilipat = $itung_jumlah_ideal_sapi*2;

                    $index_stess_sedang =0;
                    if ($luas->jumlah_sapi<=$itung_jumlah_ideal_sapi){
                        $index_stess_sedang = 1;
                    }elseif ($luas->jumlah_sapi >= $tambah_setengah_ideal){
                        $index_stess_sedang = 3;
                    }elseif ($luas->jumlah_sapi >= $duakalilipat){
                        $index_stess_sedang = 5;
                    }elseif ($luas->jumlah_sapi <= $tambah_seperempat_ideal){
                        $index_stess_sedang = 2;
                    }elseif ($luas->jumlah_sapi <= $tambah_tigaperempat_ideal){
                        $index_stess_sedang = 4;
                    }
                    $kmean_sedang[] = [
                        $peternakan->nama_peternakan,$index_stess_sedang];

//                    dd($itung_jumlah_ideal_sapi);
//                    dd($luas);
//                    dd($tenah);
                }

                foreach ($burukTernak as $zx){
//                    dd($zx);
                    $tenah = Peternakan::find($zx);
                    $luas = inputPeternak::where('peternakan_id',$tenah->id)->first();
                    $peternakan = Peternakan::find($tenah->id);

                    $itung_jumlah_ideal_sapi = floor(($luas->luas_peternakan/10));
                    $tambah_setengah_ideal = ($itung_jumlah_ideal_sapi/2) + $itung_jumlah_ideal_sapi;
                    $tambah_seperempat_ideal = ($itung_jumlah_ideal_sapi*0.25) + $itung_jumlah_ideal_sapi;
                    $tambah_tigaperempat_ideal = ($itung_jumlah_ideal_sapi*0.75) + $itung_jumlah_ideal_sapi;
                    $duakalilipat = $itung_jumlah_ideal_sapi*2;

                    $index_stess =0;
                    if ($luas->jumlah_sapi<=$itung_jumlah_ideal_sapi){
                        $index_stess = 1;
                    }elseif ($luas->jumlah_sapi >= $tambah_setengah_ideal){
                        $index_stess = 3;
                    }elseif ($luas->jumlah_sapi >= $duakalilipat){
                        $index_stess = 5;
                    }elseif ($luas->jumlah_sapi <= $tambah_seperempat_ideal){
                        $index_stess = 2;
                    }elseif ($luas->jumlah_sapi <= $tambah_tigaperempat_ideal){
                        $index_stess = 4;
                    }
                    $kmean_buruk[] = [
                        $peternakan->nama_peternakan,$index_stess];

//                    dd($itung_jumlah_ideal_sapi);
//                    dd($luas);
//                    dd($tenah);
                }
//                dd($kmean);

//                dd($kmean_sedang);

            } else{
                $data = Deteksi::limit(5)->get();

                $kmean_baik = [];
                $kmean_sedang = [];
                $kmean_buruk = [];
                $buruk = 0;
                $sedang = 0;
                $baik = 0;
            }
//            dd($buruk);

//            dd($kmean_buruk);

            if($request->ajax()) {
                $data = KualitasPeternak::with('peternakan');

                return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('name', function($data){

                        $a = '<a href="'.route('peternak',$data->peternakan_id).'">'.$data->peternakan->code_peternak.'</a>';
                        return $a;
                    })
                    ->addColumn('Kualitas_Pagi', function($data){
                        $sa = Deteksi::where('peternakan_id',$data->peternakan_id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                        $biruPagi = 0;
                        foreach ($sa as $a){
                            $timeDetail = Carbon::parse($a->time)->format('H');

                            if ($timeDetail >= '07' && $timeDetail <= '13'){
                                if ($a->warna == "Biru"){
                                    $biruPagi++;
                                }
                            }

                        }

                        if ($biruPagi >= 6 ){
                            $warnaPagi = 'bagus';
                            $scorePagi = 3;
                        }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                            $warnaPagi = 'sedang';
                            $scorePagi = 2;
                        }elseif ($biruPagi == 0){
                            $warnaPagi = null;
                            $scorePagi = 0;
                        } else{
                            $warnaPagi = 'buruk';
                            $scorePagi = 1;
                        }
//

//                        if ($timeDetail >= '16' && $timeDetail <= '21'){
//
//                        }
                        return $warnaPagi;
                    })
                    ->addColumn('Kualitas_Sore', function($data){
                        $sa = Deteksi::where('peternakan_id',$data->peternakan_id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                        $biruSore   = 0;

                        foreach ($sa as $a){
                            $timeDetail = Carbon::parse($a->time)->format('H');

                            if ($timeDetail >= '16' && $timeDetail <= '21'){
                                if ($a->warna == "Biru"){
                                    $biruSore++;
                                }
                            }

                        }

                        if ($biruSore >= 6 ){
                            $warnaSore = 'bagus';
                            $scoreSore = 3;
                        }elseif ($biruSore >= 4 && $biruSore <= 5){
                            $warnaSore = 'sedang';
                            $scoreSore = 2;
                        }elseif ($biruSore == 0){
                            $warnaSore = null;
                            $scoreSore = 0;
                        }else{
                            $warnaSore = 'buruk';
                            $scoreSore = 1;
                        }

                        return $warnaSore;

                    })
                    ->addColumn('Liter_Total', function($data){
                        $a = $data->Liter_Pagi + $data->Liter_Sore;
                        return '<div class="row p-1">' . $a . '</div>';
                    })
                    ->addColumn('Kualitas_Total', function($data){
                        $sa = Deteksi::where('peternakan_id',$data->peternakan_id)->whereDate('time', $data->Date)->get();
//                    echo 'tgl Dinamis : '.$time.' '.$timeDetail.'<br>'.'tgl fix : '.$tgl.'<br>'.'<br>'.'<br>';
                        $biruPagi = 0;
                        $biruSore = 0;
                        foreach ($sa as $a){
                            $timeDetail = Carbon::parse($a->time)->format('H');

                            if ($timeDetail >= '07' && $timeDetail <= '13'){
                                if ($a->warna == "Biru"){
                                    $biruPagi++;
                                }
                            }
                            if ($timeDetail >= '16' && $timeDetail <= '21'){
                                if ($a->warna == "Biru"){
                                    $biruSore++;
                                }
                            }


                        }

                        if ($biruPagi >= 6 ){
                            $warnaPagi = 'bagus';
                            $scorePagi = 3;
                        }elseif ($biruPagi >= 4 && $biruPagi <= 5){
                            $warnaPagi = 'sedang';
                            $scorePagi = 2;
                        }elseif ($biruPagi == 0){
                            $warnaPagi = null;
                            $scorePagi = 0;
                        } else{
                            $warnaPagi = 'buruk';
                            $scorePagi = 1;
                        }


                        if ($biruSore >= 6 ){
                            $warnaSore = 'bagus';
                            $scoreSore = 3;
                        }elseif ($biruSore >= 4 && $biruSore <= 5){
                            $warnaSore = 'sedang';
                            $scoreSore = 2;
                        }elseif ($biruSore == 0){
                            $warnaSore = null;
                            $scoreSore = 0;
                        }else{
                            $warnaSore = 'buruk';
                            $scoreSore = 1;
                        }

                        $rataSocre  =ceil (($scorePagi +$scoreSore) /2);

                        $warnaFix = '';
                        if ( ceil($rataSocre) == 3 ){
                            $warnaFix = 'bagus';
                        }elseif (ceil($rataSocre) == 2){
                            $warnaFix = 'sedang';

                        }elseif (ceil($rataSocre) == 1){
                            $warnaFix = 'buruk';

                        }
                        return '<div class="row p-1">' .$warnaFix. '</div>';
                    })
                    ->rawColumns(['name','Kualitas_Pagi','Kualitas_sore','Liter_Total','Kualitas_Total'])
                    ->make(true);
            }

            view()->share([
                'buruk' => $buruk,
                'sedang' => $sedang,
                'baik' => $baik,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'kmean_baik' => $kmean_baik,
                'kmean_sedang' => $kmean_sedang,
                'kmean_buruk' => $kmean_buruk,
            ]);

            return view('admin.content.dashboard.coba');
        }
    }
}
