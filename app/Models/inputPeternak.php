<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class inputPeternak extends Model
{
    use HasFactory;
    protected $table = 'input_peternak';

    protected $primaryKey = 'id';
    public $timestamps = false;

}
