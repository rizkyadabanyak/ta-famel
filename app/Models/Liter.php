<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Liter extends Model
{
    use HasFactory;
    protected $table = 'kualitas_peternak';
    protected $primaryKey = 'id';

    public $timestamps = false;

}
