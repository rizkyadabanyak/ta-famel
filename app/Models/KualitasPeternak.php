<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KualitasPeternak extends Model
{
    use HasFactory;

    protected $table = 'kualitas_peternak';
    protected $primaryKey = 'id';

    public function peternakan(){
        return $this->belongsTo(Peternakan::class);
    }

}
