<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Peternakan extends Model
{
    use HasFactory;
    protected $table='peternakan';
    protected $primaryKey = 'id';

    public function inputPeternak(){
        return $this->hasMany(inputPeternak::class,'peternakan_id');
    }
}
