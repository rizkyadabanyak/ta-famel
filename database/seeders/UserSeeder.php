<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new User();
        $data->name = 'peternak';
        $data->email = 'peternak@gmail.com';
        $data->password = Hash::make('112233');
        $data->level = 'peternak';
        $data->save();

        $pengunjung = new User();
        $pengunjung->name = 'pengunjung';
        $pengunjung->email = 'pengjunung@gmail.com';
        $pengunjung->password = Hash::make('112233');
        $pengunjung->level = 'pengunjung';
        $pengunjung->save();
    }
}
