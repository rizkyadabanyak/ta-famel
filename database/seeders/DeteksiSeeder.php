<?php

namespace Database\Seeders;

use App\Models\Deteksi;
use App\Models\Liter;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DeteksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $month = '2022-12';
        $start = Carbon::parse($month)->startOfMonth();
        $end = Carbon::parse($month)->endOfMonth();
//        dd($end);
        $dates = [];
        $k = 1;
        while ($start->lte($end)) {
//            $dates[] = $start->copy();
//            $start->addDay();
            $ran = ['Biru','Putih'];

            for($i=1;$i<=10;$i++){
                $randomElement = $ran[array_rand($ran, 1)];

                $data = new Deteksi();
                $data->peternakan_id = $i;
                $data->time = '2022-12-'.$k.' 21:00:21';
                $data->warna = $randomElement;
                $data->save();

            }
            $k++;

        }


    }
}
