<?php

namespace Database\Seeders;

use App\Models\Deteksi;
use App\Models\Liter;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LiterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $month = '2022-12';
        $start = Carbon::parse($month)->startOfMonth();
        $end = Carbon::parse($month)->endOfMonth();
//        dd($end);
        $dates = [];
        $k = 1;

        while ($start->lte($end)) {
//            $dates[] = $start->copy();
//            $start->addDay();
            $ran = ['Biru','Putih'];
            $ranAngka = [23,41,23,77,23,41,22,42,52];


            for($i=1;$i<=10;$i++){

                $liter  = new Liter();
                $liter->peternakan_id = $i;
                $liter->Date = '2022-12-'.$k;
                $liter->Liter_Pagi = $ranAngka[array_rand($ranAngka, 1)];
                $liter->Liter_Sore = $ranAngka[array_rand($ranAngka, 1)];
                $liter->save();

            }


            $k++;

        }
    }
}
